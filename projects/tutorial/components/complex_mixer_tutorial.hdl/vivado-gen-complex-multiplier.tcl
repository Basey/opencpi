# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

set ip_name       [lindex $argv 0]
set ip_part       [lindex $argv 1]
set ip_module ${ip_name}_0

# Create Project
create_project managed_ip_project managed_ip_project -ip -force -part $ip_part 
set_property target_language VHDL [current_project]

# Create IP
create_ip -name cmpy -vendor xilinx.com -library ip -module_name $ip_module

set_property CONFIG.ACLKEN {true} [get_ips $ip_module]
set_property CONFIG.ARESETN {true} [get_ips $ip_module]

set ip_dir managed_ip_project/managed_ip_project.srcs/sources_1/ip/$ip_module
generate_target all [get_files $ip_dir/$ip_module.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] $ip_dir/$ip_module.xci]

launch_runs ${ip_module}_synth_1 -jobs 4
wait_on_run ${ip_module}_synth_1 
open_run ${ip_module}_synth_1 -name ${ip_module}_synth_1 

write_edif -security_mode all ../$ip_module.edf -force
write_vhdl -mode synth_stub ../${ip_module}\_stub.vhd -force
write_vhdl ../$ip_module\_sim.vhd -force

## ADSB_APP

The adsb_app example application provides an OpenCPI enabled capability to receive and process the Automatic Dependent Surveillance-Broadcast (ADS-B) aviation surveillance protocol transmitted from commercial and military aircraft on a 1090 MHz carrier. The broadcast signal is used to identify aircraft and their geographical positions.  The application demodulates the protocol’s Pulse-Position Modulation (PPM) symbols, verifies the packet's cyclic redundancy check (CRC), and formats the packet data to be sent to an open source tracking application. The adsb_app application is implemented with OpenCPI HDL components found in the ocpi.comp.sdr and ocpi.examples libraries and shown in the figure below.
  
The validated received ADS-B packets may be sent to any supported ADS-B decoder and mapping tool.  For demonstration purposes, instruction to install and execute the tar1090 application found at github.com/wiedehopf/adsb-scripts are provided below.

![Figure 1. HDL Component Assembly](./fig/process_path.png)

At this time, the only supported OpenCPI hardware platforms are: e31x, zed, zcu104, and plutosdr.

NOTE: The following instructions assume that the user has successfully installed the OpenCPI application and installed and registered the ocpi.comp.sdr component library. It also assumes that the user has successfully installed the OpenCPI supported platforms.   
For example purposes, only the ZedBoard platform will be shown but similar execution commands may replace the `zed` platform with `e31x`,`zcu104` or `plutosdr`. Similarly, the `ubuntu22_04` platform will be demonstrated for host-based components but could be any supported OpenCPI host platform.

Install and Build SDR Components
---
Install and build the SDR components if not already done so.
```sh
   $ cd ~/opencpi/projects/comps
   $ git submodule update --init --remote ocpi.comp.sdr
   $ cd ocpi.comp.sdr
   $ ocpidev register project
   $ ocpidev build -d hdl/primitives --hdl-platform zed
   $ ocpidev build --rcc-platform ubuntu22_04
```
  Note: Only the primtives need to be built at this point for HDL components.

Build Examples Components
---
```sh
   $ cd ~/opencpi/projects/examples/hdl/primitives
   $ ocpidev build --hdl-platform zed
   $ cd ~/opencpi/projects/examples/components/
   $ ocpidev build --rcc-platform ubuntu22_04
```
Note: HDL components do not need to be individual built at this time.  

Build Digital Radio Controller
---
```sh
   $ cd ~/opencpi/projects/examples/hdl/devices/drc_fmcomms.rcc/
   $ ocpidev build --rcc-platform ubuntu22_04
```
Build Assembly
---

Note: Using the `--workers-as-needed` option will build the required HDL components on the fly. This precludes the necessity of building each one individually at a time from within the library.  
```sh
   $ cd ~/opencpi/projects/examples/hdl/assemblies/adsb_app/adsb_app_rx_hdl_assy/
   $ ocpidev build --hdl-platform zed --workers-as-needed
```
Build Application
---
```sh
   $ ~/opencpi/projects/examples/applications/adsb_app
   $ ocpidev build --rcc-platform ubuntu22_04
```
Install Tar1090 Service
---
The user may configure and install any ADS-B graphing tool desired. For quick reference, we selected the use of the readsb decoder and tar1090 web interface provided by wiedehophf. We show the instructions for executing this service using Ubuntu or Rocky host platforms. Using Rocky is more involved and relies on running a docker image.

## *Ubuntu 20.04 and Ubuntu 22.04*
```sh
   $ sudo bash -c "$(wget -O - https://github.com/wiedehopf/adsb-scripts/raw/master/readsb-install.sh)"
   $ sudo vi /etc/default/readsb
             Comment out RECEIVER_OPTIONS:
             #RECEIVER_OPTIONS="--device 0 --device-type rtlsdr --gain auto --ppm 0"
   $ sudo systemctl restart readsb.service
```
The tar1090 application may be accessed from a web browser at:
	**http://localhost/tar1090**

## *Rocky 8 and 9*
Install docker and run tar1090 image.
```sh
   $ sudo dnf check-update
   $ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
   $ sudo dnf install docker-ce docker-ce-cli containerd.io
   $ sudo systemctl start docker

 Only if you want to have docker run at startup then:
   $ sudo systemctl enable docker

 Run tar1090 docker image:
   $ sudo docker run -d \
    --name=tar1090 \
    -p 8078:80 \
    -e BEASTHOST=readsb \
    -p 30001:30001 \
    -v /opt/adsb/tar1090/graphs1090:/var/lib/collectd \
    --tmpfs=/run:exec,size=64M \
    --tmpfs=/var/log \
    ghcr.io/sdr-enthusiasts/docker-tar1090:latest

To rerun image or change parameters, the docker must be stopped and removed:
    $ sudo docker container stop tar1090
    $ sudo docker container rm tar1090
``` 
To access web service, use link:
	**http://localhost:8078**

Platform Preparation
---

The PlutoSDR has a default IP address of 192.168.2.1 and can be used immediately once the platform is booted. The ZedBoard and E310 do not have a default IP address and must be set prior to use. We will continue to use the `zed` platform as an example but approach must be used for both E310, ZCU104, and PlutoSDR. We will use the following to indicate the host and remote terminal prompts:

	(local host) :     ~$
	(remote platform): ~# 

Power up ZedBoard with USB connection to local host. The interface will usually register as a device with tag /dev/ttyACM0. The E310 is usually /dev/ttyUSB0.  Use a serial interface application such as ‘screen’ to access the platform device. Both the default login and password for ZedBoard and E310 is: “root”.  The login and password for PlutoSDR is ‘root’ and ‘analog’.
```sh
   ~$ sudo screen /dev/ttyACM0 115200
```
Once you have logged in, set the IP address and exit the serial interface.
```sh
   ~# ifconfig eth0 192.168.2.2
   ~# Ctrl-a  d
```
Set OCPI_SERVER_ADDRESSES environment variable to match ZedBoard IP address and set port to 12345.
```sh
   ~$ export OCPI_SERVER_ADDRESSES=192.168.2.2:12345
```
Determine the associated network interface name and set as the opencpi socket interface.  As an example, we use enx001.
```sh
   ~$ export OCPI_SOCKET_INTERFACE=enx001
```
The local socket must also have an IP address that is in the same subnet as the platform.  IF not, it would need to be set as shown below:
```sh
   ~$ ifconfig enx001 192.168.2.10
```
From the host, load the platform sandbox.
```sh
   ~$ ocpiremote load -w zed -s xilinx24_1_aarch32 -p root
   ~$ ocpiremote start -b -p root
```
Verify platform.
```sh
   ~$ ocpirun -C
```
If successfully started, then the following output will be displayed on host:

![Figure 2. Active OpenCPI Platforms](./fig/ocpirun-C.png)

Now that there is a network connection from host to platform, a user may use a serial interface or ssh to access remote platform. We will demonstrate using ssh. Login and password for ZedBoard still is `root`. The password may be asked for twice which is a known issue for this platform processor.
```sh
   ~$ ssh root@192.168.2.2
```
Move to sandbox if access to the platform was successful.
```sh
   ~# cd sandbox
```
Modify system.xml file. 

This is required to support high throughput access of output ADS-B packets. The default output buffer size is 2. The ADSB_APP specifies 10 buffers which requires the default platform SMB parameters to be modified. Edit the file and change the following:

    line 11:  <transfer smbsize=’128k’ to ‘1024k’>
    line 12: <pio load=’1’ smbsize=’10M’ to ‘100M’>
 
Exit ssh.

Re-start Platform.
```sh
   ~$ ocpiremote restart -b -p root
```
The platform should now be available for executing the ADS-B application.

Execute Application
---

The ACI based executable initiates the adsb_app.xml application and is responsible for sending the output packets recovered from the platform ADS-B processing path to the tar1090 web application. 

A limited set of options are available to display ADS-B raw packet data, verbose output, and a timeout set in seconds. The executable is found in the platform target directory
```sh
Options:
 --help                Display options
 --display             Display ADS-B Packets
 --verbose             Show ocpirun debug
 --timeout             Duration to run application (seconds), [default = -1] (no timeout)
```
```sh
   $ cd ~/opencpi/projects/examples/applications/adsb_app
   $ export OCPI_LIBRARY_PATH=../../artifacts
   $ ./target-ubuntu22_04/adsb_app --display
```
NOTE: If running in Rocky 8 or 9, then you may need to turn off or configure firewall.
```sh
        $ sudo systemctl stop firewalld.service
```
The --display option will show the crc validated ADS-B packets received:
```sh
	To tar1090 [40457]: 8d a5 97 ee 99 d 9e 94 40 4 36 21 3e 31
	To tar1090 [40458]: 8d ab 68 f9 ea 36 c8 58 1 3c 8 7d 3c ce
	To tar1090 [40459]: 8d a5 26 e2 99 d 8a 9 38 4 30 d1 f3 e0
	To tar1090 [40460]: 8d a0 f7 3c e1 16 14 0 0 0 0 3d 15 f9
	To tar1090 [40461]: 8d a0 8a 1a 58 9b 97 f9 e8 a8 77 a4 99 19
	To tar1090 [40462]: 8d a3 dc 34 23 10 13 37 e3 88 20 f6 3 d4
	To tar1090 [40463]: 8d a8 5c aa 99 10 f5 32 c8 44 34 96 57 4b
	To tar1090 [40464]: 8d ab 3e dd 58 ab 3 f9 f9 ca 55 57 40 8e
```
The tar1090 application is shown in the next figure.  The aircraft in the vicinity may now be tracked assuming user has adequate antenna. 

![Figure 3. Tar1090 ADS-B Tarck Display](./fig/tar1090.png)


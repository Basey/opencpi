-- crc_validator_uc HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc_validator_impl is
  Generic (
    dataWidth       : integer;           
    messageWidth    : integer;            
    CRCdataWidth    : integer;          
    CRCbitWidth     : integer;           
    numInWords      : integer;            
    numOutWords     : integer;           
    polyValue       : integer );    
  Port ( 
        clk         : in std_logic;
        reset       : in std_logic;
        data        : in std_logic_vector((dataWidth - 1) downto 0);
        valid       : in std_logic;
        eof         : in std_logic;
        take        : out std_logic;
        message     : out std_logic_vector((messageWidth - 1) downto 0);
        message_vld : out std_logic;
        message_rdy : in std_logic;
        message_som : out std_logic;
        message_eom : out std_logic;
        message_eof : out std_logic;
        crcMessages : out std_logic_vector(31 downto 0) );
end crc_validator_impl;


architecture Structural of crc_validator_impl is

component wordPack_impl 
Generic (
    dataWidth       : integer;
    CRCdataWidth    : integer;
    numInWords      : integer );
Port (
    clk         : in std_logic;
    reset       : in std_logic;
    input       : in std_logic_vector((dataWidth - 1) downto 0);
    vld_in      : in std_logic;
    output      : out std_logic_vector((CRCdataWidth - 1) downto 0);
    vld_out     : out std_logic;
    packStart   : in std_logic;
    packDone    : out std_logic );
end component;

component crcCal_impl
 Generic (
    CRCdataWidth    : integer;
    CRCbitWidth     : integer;
    polyValue       : integer ); 
Port ( 
    clk         : in std_logic; 
    reset       : in std_logic;
    data        : in std_logic_vector((CRCdataWidth - 1) downto 0); 
    data_valid  : in std_logic;
    message     : out std_logic_vector((CRCdataWidth - 1) downto 0);
    message_vld : out std_logic;
    crcStart    : in std_logic;
    crcDone     : out std_logic );
end component;

component wordUnpack_impl 
Generic (
    CRCmessageWidth : integer;
    messageWidth    : integer;
    numOutWords     : integer );
PORT (  
    clk         : in std_logic;
    reset       : in std_logic;
    input       : in std_logic_vector((CRCmessageWidth - 1) downto 0);
    vld_in      : in std_logic;
    eof_in      : in std_logic;
    output      : out std_logic_vector((messageWidth - 1) downto 0);
    output_vld  : out std_logic;
    output_rdy  : in std_logic;
    som         : out std_logic;
    eom         : out std_logic;
    eof_out     : out std_logic;
    numOutputs  : out std_logic_vector(31 downto 0);
    unpackStart : in std_logic;
    unpackDone  : out std_logic );
end component;

    subtype FSM_t is std_logic_vector(1 downto 0);
    constant  packProcess               : FSM_t := "00";
    constant  crcProcess                : FSM_t := "01";
    constant  unpackProcess             : FSM_t := "10";

    attribute mark_debug : string;

    signal    currState, nxtState       : FSM_t;
    attribute mark_debug of currState : signal is "TRUE";
    attribute mark_debug of nxtState : signal is "TRUE";
    
    signal    packStart, packDone       : std_logic;

    signal    crcStart, crcDone         : std_logic;
    attribute mark_debug of crcStart : signal is "TRUE";
    attribute mark_debug of crcDone  : signal is "TRUE";

    signal    unpackStart, unpackDone   : std_logic;
    attribute mark_debug of unpackStart : signal is "TRUE";
    attribute mark_debug of unpackDone : signal is "TRUE";

    signal    input_data          : std_logic_vector((dataWidth - 1) downto 0);

    signal    input_valid         : std_logic;

    signal    data_buffer         : std_logic_vector((CRCdataWidth - 1) downto 0); 

    signal    data_vld            : std_logic;

    signal    message_buffer      : std_logic_vector((CRCdataWidth - 1) downto 0);
    
    signal    message_buffer_vld  : std_logic;

begin

        input_data <= data;
        input_valid <= valid;

curStProc: process(clk)
begin
    if (rising_edge(clk)) then
        if (reset = '1') then
            currState <= packProcess;
         else
             currState <= nxtState;
         end if;
    end if;
end process;

nxtStProc: process(currState, packDone, crcDone, unpackDone)
begin
    case (currState) is
        when packProcess =>
           if (packDone = '0') then
                packStart <= '1';
                crcStart <= '0';
                unpackStart <= '0';
                nxtState <= packProcess;
            else
                packStart <= '0';
                nxtState <= crcProcess;
           end if;
        when crcProcess =>
           if (crcDone = '0') then 
                packStart <= '0';
                crcStart <= '1';
                unpackStart <= '0';
                nxtState <= crcProcess;
            else
                crcStart <= '0';
                nxtState <= unpackProcess;
            end if;
       when unpackProcess =>
           if(unpackDone ='0') then
                packStart <= '0';
                crcStart <= '0';
                unpackStart <= '1';
                nxtState <= unpackProcess;
            else
                unpackStart <= '0';
                nxtState <= packProcess;
           end if;
       when others =>
           null;
   end case;
end process nxtStProc;

outStProc: process(nxtState)
begin
    case (nxtState) is
       when packProcess =>
            take <= '1';
       when crcProcess =>
            take <= '0';
       when unpackProcess =>
            take <= '0';
       when others =>
            null;
    end case;
end process outStProc;

packProc: wordPack_impl 
generic map (
    dataWidth    => dataWidth,
    CRCdataWidth => CRCdataWidth,
    numInWords   => numInWords )
port map(
    clk       => clk,
    reset     => reset,
    input     => input_data,
    vld_in    => input_valid, 
    output    => data_buffer,
    vld_out   => data_vld,
    packStart => packStart,
    packDone  => packDone );


crcProc: crcCal_impl 
generic map(
    CRCdataWidth => CRCdataWidth,
    CRCbitWidth  => CRCbitWidth,
    polyValue    => polyValue )
port map(
    clk          => clk,
    reset        => reset,
    data         => data_buffer,
    data_valid   => data_vld,
    message      => message_buffer,
    message_vld  => message_buffer_vld,
    crcStart     => crcStart,
    crcDone      => crcDone );


unpackProc: wordUnpack_impl 
generic map (
    --CRCmessageWidth => CRCdataWidth - CRCbitWidth,
    CRCmessageWidth => CRCdataWidth,
    messageWidth    => messageWidth,
    numOutWords     => numOutWords )
port map (
    clk         => clk,
    reset       => reset,
    input       => message_buffer,
    vld_in      => message_buffer_vld,
    eof_in      => eof,
    output      => message,
    output_vld  => message_vld,
    output_rdy  => message_rdy,
    som         => message_som,
    eom         => message_eom,
    eof_out     => message_eof,
    numOutputs  => crcMessages,
    unpackStart => unpackStart,
    unpackDone  => unpackDone );

end Structural;

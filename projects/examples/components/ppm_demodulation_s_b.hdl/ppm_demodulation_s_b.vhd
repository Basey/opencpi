--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all; 
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library protocol_util; use protocol_util.all;

architecture rtl of worker is

    -- Worker control signals.
    signal valid_data     : std_logic;
    signal do_work        : std_logic;
  
    -- Output from this worker (i.e., input to the ns marshaller).
    signal output_data    : std_logic_vector(output_out.data'length-1 downto 0);

begin

    do_work          <= input_in.valid and output_in.ready;
    output_out.data  <= output_data;
    output_out.valid <= valid_data;
    output_out.eof   <= input_in.eof;    
    input_out.take   <= do_work;

    -----------------------------------
    -- Worker Implementation
    -----------------------------------
    ppm_demod: process(ctl_in.clk)
        variable first_bit_acc    : integer := 0;
        variable second_bit_acc   : integer := 0;
        variable index_counter    : integer := 0;
        constant half_sps         : integer := to_integer(samps_per_sym)/2;
    begin
        if rising_edge(ctl_in.clk) then
            if (ctl_in.reset = '1') then
                first_bit_acc     := 0;
                second_bit_acc    := 0;
                index_counter     := 0;
                valid_data        <= '0';
                output_data       <= (others=>'0');

            else
                if (do_work = '1') then
                    -- This signal would normally be in the control signal process;
                    -- but, for this worker, it's dependant on variables local to this one.
                    valid_data <= '0';

                    -- Restart counters on a new message.
                    if (input_in.som = '1') then
                        index_counter  := 0;
                        first_bit_acc  := 0;
                        second_bit_acc := 0;
                    end if;
                    -- Accumulate data. This will take a couple clock cycles since it's a
                    -- 16-bit input and an 8-bit output.
                    if (index_counter < half_sps) then
                        first_bit_acc := first_bit_acc + to_integer(signed(input_in.data));
                    else
                        second_bit_acc := second_bit_acc + to_integer(signed(input_in.data));
                    end if;

                    -- Determine whether to write logic high/low.
                    if (first_bit_acc > second_bit_acc) then
                        output_data(0) <= not std_logic(one_has_leading_zero);
                    else
                        output_data(0) <= std_logic(one_has_leading_zero);
                    end if;

                    -- Check if one sym has gone through. If it has, restart
                    if (index_counter = samps_per_sym-1) then
                        index_counter := 0;
                        valid_data <= '1';
                        first_bit_acc := 0;
                        second_bit_acc := 0;
                    else
                        index_counter := index_counter + 1;
                    end if;

                elsif (output_in.ready and valid_data) then
                    valid_data <= '0';
                end if;
            end if;
        end if;
    end process ppm_demod;

end rtl;


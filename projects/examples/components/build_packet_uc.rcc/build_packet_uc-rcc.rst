.. build_packet_uc RCC worker


.. _build_packet_uc-RCC-worker:


``build_packet_uc.rcc`` RCC Worker
==================================
Implemented only as an RCC worker.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::

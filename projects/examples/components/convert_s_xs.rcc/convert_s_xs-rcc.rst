.. convert_s_xs RCC worker


.. _convert_s_xs-RCC-worker:


``convert_s_xs.rcc`` RCC Worker
===============================
Converts input samples from protocol 'short_timed_sample-prot' to 'complex_short_timed_sample-prot'.

Detail
------
.. ocpi_documentation_worker::

.. Skeleton comment: If not a HDL worker / implementation then the below
   section and directive should be deleted. This comment should be removed in
   the final version of this page.

Utilization
-----------
.. ocpi_documentation_utilization::

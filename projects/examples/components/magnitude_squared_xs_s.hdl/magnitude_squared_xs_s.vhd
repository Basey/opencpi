-- magnitude_squared_xs_s HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library protocol_util; use protocol_util.all;

architecture OCPI_Wrapper of worker is

  component cplxtomagsq_impl
    Generic (
      sampWidth   : integer);    
    Port (
      smp_clk_in      : in std_logic;
      srst_in         : in std_logic;
      a_q15_in        : in std_logic_vector((sampWidth - 1) downto 0);
      b_q15_in        : in std_logic_vector((sampWidth - 1) downto 0);
      vld_in          : in std_logic;
      magsq_q15_out   : out std_logic_vector((sampWidth - 1) downto 0);
      magsq_out_vld   : out std_logic;
      magsq_out_rdy   : in std_logic);
  end component;

  --Worker Signals
  signal do_work                      : std_logic;
  signal valid_eom                    : std_logic;
  signal valid_data                   : std_logic;
  signal a_q15_in_cplxtomagsq         : std_logic_vector((to_integer(sampWidth) - 1) downto 0);
  signal b_q15_in_cplxtomagsq         : std_logic_vector((to_integer(sampWidth) - 1) downto 0);
  signal magsq_q15_out_cplxtomagsq    : std_logic_vector((to_integer(sampWidth) - 1) downto 0);
  signal magsq_out_vld_cplxtomagsq    : std_logic;

  -- Marshaller signals
  signal reset              : std_logic;
  signal input_sample_data  : std_logic_vector(input_in.data'length-1 downto 0); -- Input Samples Data
  signal input_sample_vld   : std_logic; -- Valid for input samples
  signal in_som             : std_logic;
  signal input_sample_eom   : std_logic; -- Eom on sample data
  signal input_eof          : std_logic;
  signal input_rdy          : std_logic; -- Tell the input that the worker is ready for data

  signal output_sample_data : std_logic_vector(output_out.data'length-1 downto 0); -- Output Samples Data
  signal output_sample_vld  : std_logic; -- Valid for output samples
  signal output_sample_eom  : std_logic; -- Samples End of Message
  signal output_eof         : std_logic;
  signal output_rdy         : std_logic; -- The output is ready for data

  signal oclk_data          : std_logic_vector(output_out.data'length-1 downto 0);

  signal iclk_opcode_num    : natural;
  signal oclk_opcode_num    : natural;

  constant gain             : std_logic_vector(3 downto 0) := (x"a"); 

begin

  ----------------------------------------
  -- OPCODE MARSHALLER
  ----------------------------------------
  -- InputOpcode Conversion
  -- Convert input opcode to something the parser can handle
  iclk_opcode_num <=
      0 when input_in.opcode = complex_short_timed_sample_sample_op_e          else
      1 when input_in.opcode = complex_short_timed_sample_time_op_e            else
      2 when input_in.opcode = complex_short_timed_sample_sample_interval_op_e else
      3 when input_in.opcode = complex_short_timed_sample_flush_op_e           else
      4 when input_in.opcode = complex_short_timed_sample_discontinuity_op_e   else
      5 when input_in.opcode = complex_short_timed_sample_metadata_op_e        else
      0;

      passthrough_marshaller : protocol_util.protocol_util.nonsample_passthrough_marshaller
      generic map(
          INPUT_DATA_WIDTH  => input_in.data'length,
          OUTPUT_DATA_WIDTH => output_out.data'length,
          WSI_MBYTEEN_WIDTH => output_out.byte_enable'length)
      port map(
          clk             => ctl_in.clk,
          rst             => ctl_in.reset,
          -- INPUT
          in_data         => input_in.data,
          in_valid        => input_in.valid,
          in_ready        => input_in.ready,
          in_som          => input_in.som,
          in_eom          => input_in.eom,
          in_opcode       => iclk_opcode_num,
          in_eof          => input_in.eof,
          in_take         => input_out.take,
          -- OUTPUT TO WORKER FROM INPUT SIDE
          samp_reset      => reset,
          samp_data_in    => input_sample_data,
          samp_in_vld     => input_sample_vld,
          samp_som_in     => in_som,
          samp_eom_in     => input_sample_eom,
          oeof            => input_eof,
          take            => input_rdy,
          -- INPUT FROM WORKER TO OUTPUT SIDE
          samp_data_out   => output_sample_data,
          samp_out_vld    => output_sample_vld,
          samp_eom_out    => output_sample_eom,
          ieof            => output_eof,
          enable          => output_rdy,
          -- OUTPUT
          out_data        => oclk_data,
          out_valid       => output_out.valid,
          out_byte_en     => output_out.byte_enable,
          out_give        => output_out.give,
          out_eom         => output_out.eom,
          out_opcode      => oclk_opcode_num,
          out_eof         => output_out.eof,
          out_ready       => output_in.ready);

  -- this only needed to avoid build bug for xsim:
  -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
  output_out.data <= oclk_data;

  -- OutputOpcode Conversion
  output_out.opcode <=
      short_timed_sample_sample_op_e          when oclk_opcode_num = 0 else
      short_timed_sample_time_op_e            when oclk_opcode_num = 1 else
      short_timed_sample_sample_interval_op_e when oclk_opcode_num = 2 else
      short_timed_sample_flush_op_e           when oclk_opcode_num = 3 else
      short_timed_sample_discontinuity_op_e   when oclk_opcode_num = 4 else
      short_timed_sample_metadata_op_e        when oclk_opcode_num = 5 else
      short_timed_sample_sample_op_e;

  ----------------------------------------
  --CONTROL SIGNALS
  ----------------------------------------
  -- Enables
  do_work <= output_rdy and input_sample_vld;
  input_rdy <= do_work when input_sample_eom = '0' else
                (do_work and valid_eom);
                
  -- Messages
  output_sample_vld <= magsq_out_vld_cplxtomagsq and not in_som;
  output_sample_data <= magsq_q15_out_cplxtomagsq;

  -- Message Boundaries
  output_sample_eom <= valid_eom and valid_data;
  output_eof <= input_eof;

  process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
        if (reset = '1') then
            valid_data <= '0';
            valid_eom  <= '0';
        elsif (do_work = '1') then
            valid_data <= '1';
            valid_eom  <= input_sample_eom;
        elsif (output_rdy = '1' and valid_data = '1') then
            valid_data <= '0';
            valid_eom  <= '0';
        end if;
    end if;
  end process;

  ----------------------------------------
  -- Worker Implementation
  ----------------------------------------
  worker_impl: cplxtomagsq_impl 
    generic map (
      sampWidth => to_integer(sampWidth))    
    port map (
      smp_clk_in => ctl_in.clk,
      srst_in => reset,
      a_q15_in => a_q15_in_cplxtomagsq,
      b_q15_in => b_q15_in_cplxtomagsq,
      vld_in => input_sample_vld,
      magsq_q15_out => magsq_q15_out_cplxtomagsq,
      magsq_out_vld => magsq_out_vld_cplxtomagsq,
      magsq_out_rdy => output_rdy);

  -- Input Port Connections
  a_q15_in_cplxtomagsq <= input_sample_data((to_integer(sampWidth) - 1) downto 0);
  b_q15_in_cplxtomagsq <= input_sample_data((to_integer(2*sampWidth) - 1) downto to_integer(sampWidth));

end OCPI_Wrapper;

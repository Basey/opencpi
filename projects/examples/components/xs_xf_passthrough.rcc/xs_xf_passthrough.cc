/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Fri Jun 28 15:24:02 2024 PDT
 * BASED ON THE FILE: xs_xf_passthrough-rcc.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the xs_xf_passthrough worker in C++
 */

#include "xs_xf_passthrough-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Xs_xf_passthroughWorkerTypes;

class Xs_xf_passthroughWorker : public Xs_xf_passthroughWorkerBase {


  void convert_data(const Complex_short_timed_sampleSampleData *in_data,
		  Complex_float_timed_sampleSampleData *out_data,
		  unsigned int in_data_length) 
  {
	  for (size_t i=0; i < in_data_length; i++) {
		float f_real = float(in_data[i].real);
		float f_imag = float(in_data[i].imaginary);
		float f_real_scaled = (f_real / (32768.));
		float f_imag_scaled = (f_imag / (32768.)); 

		out_data[i].real = f_real_scaled;
		out_data[i].imaginary = f_imag_scaled; 
	  }
  }

  RCCResult run(bool /*timedout*/) 
  {
    switch (in.opCode())
    {
	    case Complex_short_timed_sampleSample_OPERATION:
	    {
		    const Complex_short_timed_sampleSampleData* inData = in.sample().data().data();
		    Complex_float_timed_sampleSampleData* outData = out.sample().data().data();
		    unsigned int num_samples = in.sample().data().size();

		    out.setOpCode(Complex_float_timed_sampleSample_OPERATION);
		    out.sample().data().resize(num_samples);
		    convert_data(inData, outData, num_samples);
	    }
	    break;

	    default:
	    {
	    }
	    break;
    }
    return RCC_ADVANCE;
 } 
};

XS_XF_PASSTHROUGH_START_INFO
XS_XF_PASSTHROUGH_END_INFO

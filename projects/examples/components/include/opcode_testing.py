#!/usr/bin/env python3
import sys
import os
#sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.sdr.assets', 'components', 'include'))

import ocpi_message
from cq15 import cq15

# Write data to a file
# Order of Opcodes: Time -> Time -> Samps -> Samps -> Time -> Flush -> Samps -> Flush -> Flush -> Samps
def write_data(file_name, data, protocol):
    messages =[]

    #Time Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('time')
    message.fract_sec = bytes(0)
    message.sec = bytes(0)
    messages += [message]


    #Time Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('time')
    message.fract_sec = bytes(0)
    message.sec = bytes(0)
    messages += [message]


    #Samples
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    for val in data:
        message.append(val)
    messages += [message]


    #Samples
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    for val in data:
        message.append(val)
    messages += [message]


    #Time Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('time')
    message.fract_sec = bytes(0)
    message.sec = bytes(0)
    messages += [message]


    #Flush Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('flush')
    messages += [message]


    #Samples
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    for val in data:
        message.append(val)
    messages += [message]
    
    
    #Flush Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('flush')
    messages += [message]


    #Flush Opcode
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    message.set_opcode('flush')
    messages += [message]


    #Samples
    message = ocpi_message.ocpi_message()
    message.set_protocol(protocol)
    for val in data:
        message.append(val)
    messages += [message]

    #Write data
    print(messages)
    print(len(messages))
    ocpi_message.to_file(file_name, messages, messages_in_file=True)


def verify_data(input_file_name, output_file_name, in_protocol, out_protocol):
    try:
        inputvals  = ocpi_message.from_file(input_file_name,  protocol=in_protocol, messages_in_file=True)
        outputvals = ocpi_message.from_file(output_file_name, protocol=out_protocol, messages_in_file=True)
    except Exception as ex:
        print('------------------------')
        print(ex)
        ocpi_message.file_overview(input_file_name)
        ocpi_message.file_overview(output_file_name)
        sys.exit(-1)

    print('=========== INPUT OPCODES')
    for i in inputvals:
        if i.get_opcode() == 'sample':
            print(i.get_opcode(), len(i))
        else:
            print(i.get_opcode())
    print('=========== OUTPUT OPCODES')
    for i in outputvals:
        if i.get_opcode() == 'sample':
            print(i.get_opcode(), len(i))
        else:
            print(i.get_opcode())
    print('===========')
    print('# of Input  Msgs:', len(inputvals))
    print('# of Output Msgs:', len(outputvals))

    if len(inputvals) != len(outputvals):
        print('Input len != Output len')
        sys.exit(-1)
    
    in_data = []
    out_data = []


    for val in range(len(inputvals)):
        if outputvals[val].get_opcode() == 'sample':
            in_data  += [inputvals[val]]
            out_data += [outputvals[val]]
        else:
            if inputvals[val] != outputvals[val]:
                print("WARNING: nonsamples input data != output data")
                print('=========== INPUT MESSAGE')
                print(inputvals[val])
                print('=========== OUTPUT MESSAGE')
                print(outputvals[val])
                print('-----------------------------------------------')
    return in_data, out_data




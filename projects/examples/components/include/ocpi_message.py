#!/usr/bin/env python3

import numpy as np
import re
import sys
import types

from q15 import q15
from cq15 import cq15

opcodes = { "sample": 0,
            "time": 1,
            "sample_interval": 2,
            "flush": 3,
            "discontinuity": 4,
            "metadata": 5 }

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Define function used for comparing bytes with precision
def uchar_almost_equal(desired, actual, precision):
    return abs(int.from_bytes(desired, byteorder='big', signed=False) - int.from_bytes(actual, byteorder='big', signed=False)) <= precision

class ocpi_message:

    #_protocol = None

    #_opcode = None

    #_data = None

    def __init__(self, protocol=None, opcode=None, data=None):

        # Initalize all expected data members
        self._protocol = None
        self._opcode = None
        self._data = None

        if protocol != None:
            self.set_protocol(protocol)
        else:
            self.set_protocol('real')

        if opcode != None:
            self.set_opcode(opcode)
        else:
            self.set_opcode(0)

        if self._opcode == 0:

            if (type(data) == list):
                for val in data:
                    self.append(val)

        elif self._opcode == 1 or self._opcode == 2:

            if data == None:
                self._data = {'fraction': 0, 'seconds': 0}

            elif type(data) != dict:
                raise TypeError("ERROR: Data expecting type 'dict'; recieved type '" + str(type(data)) + "'.")

            else:
                if len(data) == 2 and 'fraction' in data.keys() and 'seconds' in data.keys():
                    if (type(data['fraction']) == int and type(data['seconds']) == int):
                        self._data = data
                    else:
                        raise TypeError("ERROR: Data must have integer values.")
                else:
                    raise ValueError("'dict' must only include keys 'fraction' and 'seconds'.")

    # Concatenate messages
    def __add__(self, other):

        new = ocpi_message(self._protocol, self._opcode, self._data)

        assert(type(other) in [ocpi_message, list, tuple, dict, set])

        if type(other) in [list, tuple, dict, set]:
            other = ocpi_message(self._protocol, self._opcode, list(other))

        if self._protocol != other._protocol:
            raise TypeError('Can\'t concatenate messages with different protocols.')

        if self._opcode != 0 or other._opcode != 0:
            raise TypeError('Can\'t concatenate non-samples messages.')

        # Just append for sanity's sake
        new.extend(other)

        return new


    def __bytes__(self):

        return self.to_bytes(byteorder='big', messages_in_file=True)


    def __eq__(self, other):

        if type(other) is not ocpi_message:
            raise Exception('ERROR: Cannot compare ocpi_message with type \'' + str(type(other)) + '\'.')

        # Messages are considered equal even if protocols do not match now
        # Compare protocol
        #if self._protocol != other._protocol:
        #    return False

        # Compare opcode
        if self._opcode != other._opcode:
            return False

        # Compare data
        if self._data != other._data:
            return False

        # Otherwise, they're the same
        return True


    def __getitem__(self, key):

        return self._data[key]


    def __len__(self):

        return self.length()


    def __repr__(self):

        return self.__str__()


    def __setitem__(self, key, value):

        # If opcode not defined yet, infer based on key
        if self._opcode == None:
            # Int key implies samples opcode
            if type(key) == int:
                self.set_opcode(opcodes['sample'])
            elif type(key) == str and (key == 'fraction' or key == 'seconds'):
                self.set_opcode(opcodes['time'])
            elif type(key) == str and (key == 'id' or key == 'value'):
                self.set_opcode(opcodes['metadata'])

        # Handle samples opcode differently for each protocol
        if self._opcode == 0:

            if self._protocol == 0:
                assert(type(value) == bytes)
                assert(len(value) == 1)
                self._data[key] = value
            elif self._protocol == 1:
                self._data[key] = q15(value)
            elif self._protocol == 2:
                self._data[key] = cq15(value)
            else:
                raise TypeError('Could not infer protocol from type: ' + str(type(value)))

        else:
            self._data[key] = value


    def __str__(self):

        s = 'OCPI Message:\n' + \
            '    Protocol: ' + str(self._protocol)

        # Add protocol name if it's defined
        if self._protocol != None:
            if self._protocol == 0:
                s += ' (uchar_timed_sample)\n'
            elif self._protocol == 1:
                s += ' (short_timed_sample)\n'
            elif self._protocol == 2:
                s += ' (complex_short_timed_sample)\n'
            else:
                s += '\n'
        else:
            s += '\n'

        s += '    Opcode:   ' + str(self._opcode)

        # Add opcode name if it's defined
        if self._opcode in opcodes.values():
            s += ' (' + list(opcodes)[self._opcode] + ')\n'
        else:
            s += ' (unknown)\n'

        s += '    Data:     '

        if self._opcode == 1 or self._opcode  == 2:
            s +=  str(self.fixed_to_float()) + 's '

        return s + str(self._data) + '\n'


    # Append data to end of list iff opcode is zero
    def append(self, value):

        # Don't allow appending lists
        if type(value) in [list, tuple, dict, set]:
            raise TypeError("Cannot append type '" + str(type(value)) + "' to message. Use 'extend()' instead.")

        # Infer opcode if not set
        if self._opcode == None:
            self.set_opcode(opcodes['sample'])
        elif self._opcode not in opcodes.values():
            # Unknown opcode, just append it, who cares
            # If someone wants me to handle it, they better give me a raise
            self._data.append(value)
            return

        elif self._opcode != 0:
            raise ValueError('Cannot append data to metadata opcode.')

        # If protocol not defined yet, infer based on value type
        if self._protocol == None:
            if type(value) == bytes:
                self._protocol = 0
            elif type(value) == q15 or type(value) == float:
                self._protocol = 1
            elif type(value) == cq15 or type(value) == complex:
                self._protocol = 2
            else:
                raise ValueError('Could not infer protocol from type: ' + str(type(value)))

        if self._protocol == 0:
            if type(value) == bytes:

                if len(value) == 1:
                    self._data.append(value)
                else:
                    raise TypeError("Cannot append more than one byte at once.")

            elif type(value) == int:

                self._data.append(value.to_bytes(length=1, byteorder='big', signed=False))

            else:
                raise TypeError("Cannot append type '" + str(type(value)) + "' to message with protocol 'uchar'.")

        elif self._protocol == 1:

            # Cast to q15 if possible
            if type(value) in [int, float, q15]:
                self._data.append(q15(value))
            else:
                raise TypeError("Cannot append type '" + str(type(value)) + "' to message with protocol 'real'.")

        elif self._protocol == 2:

            # Cast to cq15 if possible
            if type(value) in [int, float, q15, cq15]:
                self._data.append(cq15(value))
            else:
                raise TypeError("Cannot append type '" + str(type(value)) + "' to message with protocol 'complex'.")

        else:
            raise InternalError("Unexpected protocol: '" + self._protocol + "'.")


    def clear(self):

        self.set_opcode(self._opcode)


    def extend(self, l):

        assert(type(l) in [list, tuple, dict, set, ocpi_message])

        if self._opcode != 0:
            raise TypeError('Cannot extend message of non-samples opcode.')

        for val in l:
            # Wacky wild python bytes
            if type(val) == int:
                val = val.to_bytes(length=1, byteorder='big', signed=False)

            self.append(val)

    # Turns the time wrapped in sample-interval/time opcodes to its floating point value.
    def fixed_to_float(self):

        if not (self._opcode == 1 or self._opcode == 2):
            raise TypeError("Only time and sample-interval opcodes have fixed-point values.")

        # The seconds are the integer part of the Q32.40 and can simply be added.
        float_time = int(np.uint32(self._data['seconds']))

        frac_str = format(np.uint64(self._data['fraction']), 'b').zfill(64) # zfill forces this to store a full 64-bit value.
        assert len(frac_str) == 64, 'Stringifying the fraction failed in fixed_to_float'

        # The bottom 3 bytes of a fractional part are garbage, according to the protocol this library
        # is written for. Ignoring them to match the protocol.
        for i in range(40):

            float_time += int(frac_str[i]) * 2**((-1)*(i+1))

        return float_time


    def index(self, elmnt):

        return self._data.index(elmnt)


    def length(self):

        if self._opcode == 0:
            return len(self._data)
        else:
            raise TypeError("Cannot get length of non-samples opcode message.")


    def get_data(self):
        if self._opcode == 0:
            return self._data
        else:
            raise TypeError("Cannot get data of a non-samples opcode message.")

    def get_opcode(self):

        return list(opcodes)[self._opcode]


    def get_protocol(self):

        if self._protocol == 0:
            return 'uchar'
        elif self._protocol == 1:
            return 'real'
        elif self._protocol == 2:
            return 'complex'

        return self._protocol


    def set_opcode(self, num):

        # Allowing unknown opcodes for now
        # Check if opcode valid
        #if type(num) == int:
        #    if num > 6 or num < 0:
        #        raise ValueError('Opcode not known.')

        # If given opcode is string, convert to corresponding int
        if type(num) == str:
            try:
                num = opcodes[num]
            except:
                raise ValueError('Opcode not known.')
        elif type(num) != int:
            raise TypeError("Opcode must be of type 'int' or 'str'.")

        # Set 'data' data member accordingly
        # Samples opcode
        if num == 0:
            self._data = [ ]
        # Time opcode
        elif num == 1:
            self._data = { 'fraction': 0,
                           'seconds': 0 }
        # Interval opcode
        elif num == 2:
            self._data = { 'fraction': 0,
                           'seconds': 0 }
        # Flush, sync, and end_of_samples opcodes
        elif 3 <= num <= 4:
            self._data = None
        # User opcode
        elif num == 5:
            self._data = { 'id': 0,
                           'value': 0 }

        # Update data member
        self._opcode = num


    def set_protocol(self, protocol):

        # Handle None type
        if protocol == None:

            # Clear protocol and data
            self._protocol = None

            # Opcode 0 needs protocol to be defined
            if self._opcode == 0:
                self._opcode = None
                self._data = None

        # Handle int type
        elif type(protocol) == int:

            if 0 <= protocol <= 2:
                self._protocol = protocol

            else:
                raise ValueError('Invalid protocol given. Valid protocols are 0 (uchar), 1 (real), and 2 (complex).')

        # Handle str type
        elif type(protocol) == str:

            # Cleanse string and infer what user meant
            protocol = protocol.lower()

            # Remove non-alphabetic characters
            protocol = re.sub('[^a-z]+', '', protocol)

            if 'char' in protocol:
                self._protocol = 0

            elif 'real' in protocol:
                self._protocol = 1

            elif 'complex' in protocol:
                self._protocol = 2

            else:
                raise ValueError("Invalid protocol given. Valid protocols are 'uchar', 'real', or 'complex'.")

        else:
            raise TypeError("Invalid type given. Expected 'NoneType', 'int', or 'str'.")

        # Now that the protocol is set, make sure all elements in 'data' are of that type
        if self._data != None and self._opcode == 0:

            old_data = self._data

            if self._protocol == 0:
                try:
                    self._data = [ bytes(val) for val in old_data ]
                except:
                    self._data = [ ]
                    print("Warning: Could not convert message data to type 'uchar'; message data cleared.")

            elif self._protocol == 1:
                try:
                    self._data = [ q15(val) for val in old_data ]
                except:
                    self._data = [ ]
                    print("Warning: Could not convert message data to type 'real'; message data cleared.")

            elif self._protocol == 2:
                try:
                    self._data = [ cq15(val) for val in old_data ]
                except:
                    self._data = [ ]
                    print("Warning: Could not convert message data to type 'complex'; message data cleared.")


    def to_bytes(self, byteorder='big', messages_in_file=True):

        message_bytes = b''

        # Write data
        if self._data != None:

            # Samples opcode
            if self._opcode == 0:

                for item in self._data:

                    if type(item) in [ q15, cq15 ]:
                        message_bytes += item.to_bytes(byteorder)
                    else:
                        message_bytes += bytes(item)

            # Time and interval opcodes
            elif self._opcode in [1, 2]:

                # Since in GPS time, fraction bits occupy MSBs, so shift over
                fraction = self._data['fraction'] << ( 3 * 8 )

                message_bytes += fraction.to_bytes(8, byteorder=byteorder, signed=False)
                message_bytes += self._data['seconds'].to_bytes(4, byteorder=byteorder, signed=False)

            # Non-data opcodes
            elif 3 <= self._opcode <= 4:

                pass

            # User-defined metadata opcode
            elif self._opcode == 5:

                message_bytes += self._data['id'].to_bytes(4, byteorder=byteorder, signed=False)
                message_bytes += self._data['value'].to_bytes(8, byteorder=byteorder, signed=False)

            # Unknown opcode
            # These opcodes are undefined, so just cast everything to bytes, it's not our problem
            else:

                for item in self._data:

                    message_bytes += item.to_bytes(byteorder)

        # Add header iff parameter is true
        if messages_in_file == True:

            message_bytes = len(message_bytes).to_bytes(4, byteorder=byteorder, signed=False) + \
                            self._opcode.to_bytes(4, byteorder=byteorder, signed=False) + \
                            message_bytes

        return message_bytes


def almost_equal(message1, message2, precision):

    if (message1.get_opcode() != message2.get_opcode()):
        return False

    if message1.get_opcode() == 'sample':
        # Figure out how to compare values based on protocols
        compare_func = None
        if message1.get_protocol() == 'uchar':
            compare_func = uchar_almost_equal
        elif message1.get_protocol() == 'real':
            compare_func = q15.almost_equal
        else:
            compare_func = cq15.almost_equal

        if (len(message1) != len(message2)):
            return False

        for i in range(0, len(message1)):
            if not compare_func(message[1], message[2], precision):
                return False

    elif message1.get_opcode() in ['time', 'sample_interval']:

        # TODO: CONVERT MESSAGE ARGUMENTS TO FLOAT

        if not message1['fraction'] - precision <= message2['fraction'] <= message1['fraction'] + precision:
            return False

        if not message1['seconds'] - precision <= message2['seconds'] <= message2['seconds'] + precision:
            return False

    return True


def assert_almost_equal(message1, message2, precision):

    if not almost_equal(message1, message2, precision):
        raise AssertionError(message1, '!=', message2)


def break_up_samples(messages, length):

    if type(messages) == ocpi_message:
        messages = [messages]

    assert(type(messages) in [list, tuple, dict, set])
    assert(type(length) == int)
    assert(length > 0)

    new_list = []
    for message in messages:

        assert(type(message) == ocpi_message)

        # Break up samples
        if message.get_opcode() == 'sample':

            while len(message) >= length:

                new_list.append(ocpi_message(message.get_protocol(), message.get_opcode(), message[:length]))
                message = ocpi_message(message.get_protocol(), message.get_opcode(), message[length:])

            new_list.append(message)

        # Otherwise append
        else:
            new_list.append(message)

    return new_list


# Takes list of ocpi_message and merges all neighboring samples messages into single message
def combine_samples(messages):

    assert(type(messages) in [list, tuple, dict, set])

    new_list = []
    current_message = ocpi_message()
    for message in messages:

        assert(type(message) == ocpi_message)

        if len(new_list) == 0:
            new_list.append(message)

        elif new_list[-1].get_opcode() == 'sample' and message.get_opcode() == 'sample':
            # MERGE
            new_list[-1] = new_list[-1] + message

        else:
            new_list.append(message)

    return new_list


def compare_messages(expected, actual, precision=4, index_range=None, length=32, preceding_vals=2):

    # This method is only designed to work with samples messages
    assert(expected.get_opcode() == 'sample')
    assert(actual.get_opcode() == 'sample')

    # Define function used for comparing bytes with precision
    def uchar_almost_equal(desired, actual, precision):
        return abs(int.from_bytes(desired, byteorder='big', signed=False) - int.from_bytes(actual, byteorder='big', signed=False)) <= precision

    # The only combination of messages we really can't compare is
    # a 'uchar' message with a 'real' or 'complex' message, so
    # throw error on that case
    if ((expected.get_protocol() == 'uchar' and actual.get_protocol() != 'uchar') or
            (actual.get_protocol() == 'uchar' and expected.get_protocol() != 'uchar')):
        raise Exception('Cannot compare \'' + expected.get_protocol() + '\' message with \'' +
                        actual.get_protocol() + '\'.')

    # Figure out how to compare values based on protocols
    compare_func = None
    if expected.get_protocol() == 'uchar' and actual.get_protocol() == 'uchar':
        compare_func = uchar_almost_equal
    elif expected.get_protocol() == 'real' and actual.get_protocol() == 'real':
        compare_func = q15.almost_equal
    else:
        compare_func = cq15.almost_equal

    # If range of values not given, find first discrepancy and start there
    if index_range == None:

        start_index = 0

        for i in range(0, max(len(expected), len(actual))):

            try:
                expected_val = expected[i]
            except:
                pass

            try:
                actual_val = actual[i]
            except:
                pass

            if not compare_func(expected_val, actual_val, precision):
                start_index = i-preceding_vals if (i-preceding_vals) >= 0 else 0
                break

        # When printing, start just before discrepancy, and continue until length reached
        index_range = range(start_index, min(start_index + length, max(len(expected), len(actual))))

    # Width of each column printed to screen
    column_widths = [ 7, 20, 20, 20 ]

    # If either of the messages' protocols are 'complex', we need more room for outputting 
    if expected.get_protocol() == 'complex':
        column_widths[1] = 40
        column_widths[3] = 40
    if actual.get_protocol() == 'complex':
        column_widths[2] = 40
        column_widths[3] = 40

    # Print table header
    row_format = '{:<' + str(column_widths[0]-1) + \
                 '} {:<' + str(column_widths[1]-1) + \
                 '} {:<' + str(column_widths[2]-1) + \
                 '} {:<' + str(column_widths[3]-1) + '}'
    print(row_format.format('Index', 'First Message', 'Second Message', 'Difference'))
    print('-' * (sum(column_widths) - 4))

    # Now compare and print messages
    for i in index_range:

        # Compile list of what should be printed
        line = [ { 'color': colors.ENDC, 'val': i},
                 { 'color': colors.ENDC, 'val': ' -'},
                 { 'color': colors.ENDC, 'val': ' -'},
                 { 'color': colors.ENDC, 'val': ' -'} ]

        try:
            line[1]['val'] = expected[i]
        except:
            pass

        try:
            line[2]['val'] = actual[i]
        except:
            pass


        if type(line[1]['val']) != str and type(line[2]['val']) != str:

            # Get difference between values
            if type(line[1]['val']) == bytes:
                line[3]['val'] = abs(int.from_bytes(line[1]['val'], byteorder='big', signed=False) -
                                     int.from_bytes(line[2]['val'], byteorder='big', signed=False)).to_bytes(byteorder='big', length=1)
            else:
                line[3]['val'] = line[1]['val'] - line[2]['val']

            # Convert to string and add appropriate color
            if line[1]['val'] == line[2]['val']:

                # Exactly the same, output green
                line[1]['color'] = colors.OKGREEN
                line[2]['color'] = colors.OKGREEN

            elif compare_func(line[1]['val'], line[2]['val'], precision):

                # Close enough, output yellow
                line[1]['color'] = colors.WARNING
                line[2]['color'] = colors.WARNING

            else:

                # Numbers do not match, output red
                line[1]['color'] = colors.FAIL
                line[2]['color'] = colors.FAIL

        # Now print formatted line
        # Since string formatting doesn't seem to ignore escape sequences like it should
        # Output spacing manually
        line_str = line[0]['color'] + str(line[0]['val']) + ' '*(column_widths[0] - len(str(line[0]['val'])))
        for i in range(1, 4):

            # Add color
            line_str += line[i]['color']

            # Add value
            line_str += str(line[i]['val'])

            # Add spaces
            line_str += ' '*(column_widths[i] - (len(str(line[i]['val']))))

        print(line_str)


# Prints overview of file messages in table form
# Mainly for detecting malformed messages
# Expects message headers to be present in file
def file_overview(filename, data_length=20):

    from prettytable import PrettyTable

    # Setup header
    table = PrettyTable(['Num', 'Valid', 'Length', 'OpCode', 'Data'])

    error_message = None

    with open(filename, 'rb') as f:

        i = 1

        while True:

            row = [i, colors.OKGREEN + '✓' + colors.ENDC, ' - ', ' - ', ' - ']

            i += 1

            # Get message length
            message_length = f.read(4)

            # If no data left, break from loop
            if len(message_length) == 0:
                break
            # Otherwise, make sure there are enough bytes
            elif len(message_length) != 4:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                row[2] = R'¯\_(ツ)_/¯'
                error_message = 'Not enough bytes left to read message length. Expected: 4, Received: ' + str(len(message_length))
                table.add_row(row)
                break
            else:
                # Write message length to row
                row[2] = int.from_bytes(message_length, byteorder=sys.byteorder, signed=False)

            # Read in opcode
            opcode = f.read(4)

            if len(opcode) != 4:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                row[3] = R'¯\_(ツ)_/¯'
                error_message = 'Not enough bytes left to read message opcode. Expected: 4, Received: ' + str(len(opcode))
                table.add_row(row)
                break
            else:
                # Write opcode to row
                row[3] = int.from_bytes(opcode, byteorder=sys.byteorder, signed=False)

                if row[3] < len(list(opcodes)):
                    row[3] = list(opcodes)[row[3]]

            # Read in data
            data = f.read(row[2])

            # Add first 4 elements of data to list for printing
            row[4] = [val.to_bytes(length=1, byteorder='big', signed=False).hex() for val in data[0:min(len(data), data_length)]]
            if row[4]:
                row[4] = '0x' + ''.join([str(val) for val in row[4]])
            else:
                row[4] = ' - '

            if len(data) > data_length:
                row[4] += ' ...'

            # If not the same amount of data as message length in header
            if len(data) != row[2]:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                error_message = 'Not enough bytes left to read all message data. Expected: ' + str(row[2]) + ', Received: ' + str(len(data))
                table.add_row(row)
                break

            # Add row to table
            table.add_row(row)

    # Justify lines before printing
    table.align['Length'] = 'l'
    table.align['OpCode'] = 'l'
    table.align['Data'] = 'l'

    print(table)

    if error_message:
        print('Error:', colors.FAIL + error_message + colors.ENDC)



# Returns list of ocpi_messages from given file
def from_file(filename, protocol, messages_in_file=False):

    # List of ocpi messages pulled from file to return
    messages = [ ]

    # Open file
    with open(filename, 'rb') as f:

        while True:

            # Declare new ocpi_message
            message = ocpi_message(protocol, 0)

            # If message_in_file is true, get message header
            if messages_in_file == True:

                # Get message length in bytes
                message_length = f.read(4)

                # Break out of loop if there's no data left
                if len(message_length) == 0:
                    break
                # Otherwise, make sure theres enough for a uint32
                elif len(message_length) != 4:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

                # Convert message length to int
                message_length = int.from_bytes(message_length, byteorder=sys.byteorder, signed=False)

                # Get opcode
                opcode = f.read(4)
                if len(opcode) != 4:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

                # Convert opcode to int
                message.set_opcode(int.from_bytes(opcode, byteorder=sys.byteorder, signed=False))

                # Get 'message_length' bytes from file
                data = f.read(message_length)
                if len(data) != message_length:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

            # Otherwise, read entire file
            else:
                # Read data
                data = f.read()
                if len(data) == 0:
                    break

                # Set opcode to default
                message.set_opcode(0)

            # If normal 'samples' opcode, operate according to protocol
            if message._opcode == 0:

                if protocol == 'uchar' or protocol == 0:

                    # Just break into pieces
                    message._data = [ data[i] for i in range(0, len(data)) ]

                elif protocol == 'real' or protocol == 1:

                    # Break data bytes into pieces and convert to q15
                    message._data = [ q15(0).from_bytes(data[i:i+2], sys.byteorder) for i in range(0, len(data), 2) ]

                elif protocol == 'complex' or protocol == 2:

                    # Break data bytes into pieces and convert to cq15
                    message._data = [ cq15(0).from_bytes(data[i:i+4], sys.byteorder) for i in range(0, len(data), 4) ]

            # Handle metadata opcodes
            # Time and interval opcode
            elif message._opcode in [1, 2]:

                # Check for proper length
                if len(data) != 12:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

                # Extract data and set data members
                # Fractional part is in Q40 format inside a 64-bit number; but, only the top 40 bits are used.
                message['fraction'] = (int.from_bytes(data[3:8], byteorder=sys.byteorder, signed=False)) << 24
                message['seconds'] = int.from_bytes(data[8:12], byteorder=sys.byteorder, signed=False)


            # No-data opcodes
            elif 3 <= message._opcode <= 4:

                # Check for proper length
                if len(data) != 0:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

            # User defined opcode
            elif message._opcode == 5:

                # Check for proper length
                if len(data) != 12:
                    raise Exception('ERROR: Malformed message in file. Use file_overview() for more information.')

                # Get uuid
                message['id'] = int.from_bytes(data[0:4], byteorder=sys.byteorder, signed=False)
                message['value'] = int.from_bytes(data[4:12], byteorder=sys.byteorder, signed=False)

            else:
                raise Exception('Unknown opcode in file.')

            # Add new ocpi_message to messages list
            messages.append(message)

    # Return list
    return messages


# Takes list of ocpi_message and returns list of ocpi_message with all samples messages removed
def parse_metadata(messages):

    assert(type(messages) in [list, tuple, dict, set])

    new_list = []
    for message in messages:

        assert(type(message) == ocpi_message)

        if message.get_opcode() != 'sample':
            new_list.append(message)

    return new_list


# Takes list of ocpi_message and returns list of ocpi_message with all metadata messages removed
def parse_samples(messages):

    assert(type(messages) in [list, tuple, dict, set])

    new_list = []
    for message in messages:

        assert(type(message) == ocpi_message)

        if message.get_opcode() == 'sample':
            new_list.append(message)

    return new_list


def to_file(filename, messages, messages_in_file=True, max_message_size=4096, append=False):

    assert(type(messages) in [ocpi_message, list])

    # Make sure messages is a list of messages
    if type(messages) == ocpi_message:
        messages = [ messages ]

    # If max_message_size set, break up samples messages
    if max_message_size > -1:
        break_up_samples(messages, max_message_size)

    # Set mode of writing
    mode = 'ab' if append==True else 'wb'

    # Open output file
    with open(filename, mode) as f:

        # Write every message in list
        for message in messages:

            # Make sure opcode is 0 when messages_in_file is false
            if messages_in_file == False and message._opcode != 0:
                print('Warning: Writing message of non-samples opcode to file while messages_in_file is False.')

            f.write(message.to_bytes(byteorder=sys.byteorder, messages_in_file=messages_in_file))


# Puts metadata messages around one message. In unit testing, this input message is intended to be a sample,
# since the returned list is an order of messages that is more likely to break things.
def wrap_in_metadata(message):

    assert type(message) == ocpi_message, 'Only ocpi_message objects can be used in this function.'

    time = ocpi_message(protocol=message.get_protocol(), opcode='time', \
                        data={'seconds': 0xABCDDEF6, 'fraction': 0x12345689FF})
    flush = ocpi_message(protocol=message.get_protocol(), opcode='flush')
    sync = ocpi_message(protocol=message.get_protocol(), opcode='discontinuity')

    return [time, time, message, message, time, flush, message, flush, flush, message, sync]



# Prints overview of file messages in table form
# Mainly for detecting malformed messages
# Expects message headers to be present in file
def file_overview(filename, data_length=20):

    from prettytable import PrettyTable

    # Setup header
    table = PrettyTable(['Num', 'Valid', 'Length', 'OpCode', 'Data'])

    error_message = None

    with open(filename, 'rb') as f:

        i = 1

        while True:

            row = [i, colors.OKGREEN + '✓' + colors.ENDC, ' - ', ' - ', ' - ']

            i += 1

            # Get message length
            message_length = f.read(4)

            # If no data left, break from loop
            if len(message_length) == 0:
                break
            # Otherwise, make sure there are enough bytes
            elif len(message_length) != 4:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                row[2] = R'¯\_(ツ)_/¯'
                error_message = 'Not enough bytes left to read message length. Expected: 4, Received: ' + str(len(message_length))
                table.add_row(row)
                break
            else:
                # Write message length to row
                row[2] = int.from_bytes(message_length, byteorder=sys.byteorder, signed=False)

            # Read in opcode
            opcode = f.read(4)

            if len(opcode) != 4:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                row[3] = R'¯\_(ツ)_/¯'
                error_message = 'Not enough bytes left to read message opcode. Expected: 4, Received: ' + str(len(opcode))
                table.add_row(row)
                break
            else:
                # Write opcode to row
                row[3] = int.from_bytes(opcode, byteorder=sys.byteorder, signed=False)

                if row[3] < len(list(opcodes)):
                    row[3] = list(opcodes)[row[3]]

            # Read in data
            data = f.read(row[2])

            # Add first 4 elements of data to list for printing
            row[4] = [val.to_bytes(length=1, byteorder='big', signed=False).hex() for val in data[0:min(len(data), data_length)]]
            if row[4]:
                row[4] = '0x' + ''.join([str(val) for val in row[4]])
            else:
                row[4] = ' - '

            if len(data) > data_length:
                row[4] += ' ...'

            # If not the same amount of data as message length in header
            if len(data) != row[2]:
                # Set 'success' column value to 'X'
                row[1] = colors.FAIL + 'X' + colors.ENDC
                error_message = 'Not enough bytes left to read all message data. Expected: ' + str(row[2]) + ', Received: ' + str(len(data))
                table.add_row(row)
                break

            # Add row to table
            table.add_row(row)

    # Justify lines before printing
    table.align['Length'] = 'l'
    table.align['OpCode'] = 'l'
    table.align['Data'] = 'l'

    print(table)

    if error_message:
        print('Error:', colors.FAIL + error_message + colors.ENDC)


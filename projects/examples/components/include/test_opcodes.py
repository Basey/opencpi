#!/usr/bin/env python3

import argparse
import random
import sys

# Get CLI arguments
parser = argparse.ArgumentParser(prog='test_opcodes')
parser.add_argument('-d', '--debug', action='store_true', help='Print various debugging information.')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-v', '--verify', action='store_true', help='Verify that given output file is correct.')
group.add_argument('-g', '--generate', action='store_true', help='Write metadata messages to given file.')
parser.add_argument('filename', nargs='*')
args = parser.parse_args()

# Only use last file given
filename = args.filename[0]

if args.debug:
    print(args)

# Generate test file
if args.generate:
    
    # Open output file
    with open(filename, 'wb') as f:

        # Write time message
        f.write(int(8).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(1).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(2624927093).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(791826347).to_bytes(length=4, byteorder=sys.byteorder))

        # Write interval message
        f.write(int(8).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(2).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(12461144431155444647).to_bytes(length=8, byteorder=sys.byteorder))

        # Write frequency message
        f.write(int(8).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(3).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(12198840943454368983).to_bytes(length=8, byteorder=sys.byteorder))

        # Write flush message
        f.write(int(0).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(4).to_bytes(length=4, byteorder=sys.byteorder))

        # Write sync message
        f.write(int(0).to_bytes(length=4, byteorder=sys.byteorder))
        f.write(int(5).to_bytes(length=4, byteorder=sys.byteorder))

        # Write end_of_samples message
        #f.write(int(0).to_bytes(length=4, byteorder=sys.byteorder))
        #f.write(int(6).to_bytes(length=4, byteorder=sys.byteorder))

        # Write unknown opcode
        #f.write(int(0).to_bytes(length=4, byteorder=sys.byteorder))
        #f.write(int(254).to_bytes(length=4, byteorder=sys.byteorder))

elif args.verify:

    # Open input file
    with open(filename, 'rb') as f:

        # Check time message
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 8)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 1)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 2624927093)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 791826347)

        # Check interval message
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 8)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 2)
        assert(int.from_bytes(f.read(8), byteorder=sys.byteorder, signed=False) == 12461144431155444647)

        # Check frequency message
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 8)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 3)
        assert(int.from_bytes(f.read(8), byteorder=sys.byteorder, signed=False) == 12198840943454368983)

        # Check flush message
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 0)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 4)

        # Check sync message
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 0)
        assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 5)

        # Check end_of_samples message
        #assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 0)
        #assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 6)

        # Check unknown message
        #assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 0)
        #assert(int.from_bytes(f.read(4), byteorder=sys.byteorder, signed=False) == 254)

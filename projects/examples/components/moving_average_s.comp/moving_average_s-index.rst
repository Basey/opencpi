.. moving_average_s documentation

.. _moving_average_s:

Moving Average (``moving_average_s``)
=====================================
Calculates the average from the input data over a moving window of values.

Design
------
A window of size ``length`` is used to calculated the average of an incoming data stream.

The mathematical representation of the implementation is given in :eq:`moving_average_rr-equation`.

.. math::
   :label: moving_average_rr-equation

   y[n] = \frac{\sum_{i=0}^{\alpha-1}x[n+i]}{\alpha}


In :eq:`moving_average_rr-equation`:

 * :math:`\alpha` is the ``length`` property value.

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

Interface
---------
.. literalinclude:: ../specs/moving_average_s-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
All opcodes excluding samples are passed directly through the worker.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../moving_average_s.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``moving_average_s`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::


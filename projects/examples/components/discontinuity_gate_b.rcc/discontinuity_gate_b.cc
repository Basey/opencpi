/*******************************************************************************
 * discontinuity_gate_b
 *
 * This worker typically functions in conjunction with the pattern_detector_b
 * worker. Since, the pattern_detector_b component generates discontinuity
 * output opcodes when it identifies a specified binary pattern on its input
 * port, this worker processes discontinuity opcodes as indicator signals when
 * it receives them from a pattern_detector_b worker. The discontinuity_gate_b
 * worker functions as a gate to allow or not allow the flow of samples from
 * its input to output. The discontinuity_gate_worker supports two input ports
 * and one output port. The first input named “input_open” classifies a
 * received discontinuity opcode as an indicator to open the gate and the second
 * input port named “input_close” classifies received discontinuity opcodes as
 * indicators to close the output port. When the gate is “open”, samples and
 * signaling flow from the ‘input_open’ port to ‘output’ port. The worker uses
 * the ‘input_close’ port only to process discontinuity opcodes to identify
 * indication to stop flow of samples.
 *
 * Note: This worker is used by the OpenCPI FSK demonstration application to
 * identify the start and stop of a data packet.    
 *
 * interfaces:
 * 	input_open:	bool_timed_sample-prot 
 * 	input_close:	bool_timed_sample-prot 
 * 	output:		bool_timed_sample-prot 
 *
 * ****************************************************************************/

#include "discontinuity_gate_b-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Discontinuity_gate_bWorkerTypes;

class Discontinuity_gate_bWorker : public Discontinuity_gate_bWorkerBase {

   bool gate_flow = false;
   bool done = false;
   int count = 0;

   // Identify initial gate starting state (open or closed)
   RCCResult pattern_written() {
       gate_flow = static_cast<uint64_t>(properties().gate_open);
   return RCC_OK;
   }
   // Note: Samples and signaling only flow from input_open port to output port.
   //       The input_close port is used only to process discontinuity opcodes
   //       to identify instruction to close sample flow. All opcodes other
   //       than sample and discontinuity are always allowed to pass to output
   //       when received on input_open port.

   RCCResult run(bool /*timedout*/) {

   /* Process 'INPUT_OPEN' port  */
   // Identify received discontinity opcode on input_open port as
   // indicator to allow samples to flow from input_open port to
   // output port.
       if(input_open.opCode()==Bool_timed_sampleDiscontinuity_OPERATION)
       {
           gate_flow = true;  //toggle gate open
           printf("discontinuity_gate_b : OPENED\n");
           input_open.advance();
       }
       //Pass through sample data only if gate_flow == true, else advance
       //input_open only.
       else if ((input_open.opCode()==Bool_timed_sampleSample_OPERATION))
       {
           size_t input_length = input_open.sample().data().size();
           output.sample().data().resize(input_length);

           const uint8_t* in_data_ptr = input_open.sample().data().data();
           uint8_t* out_data_ptr = output.sample().data().data();
           int length_in_bits = input_length * sizeof(in_data_ptr[0]);
           // Only advance output when gate is identified as open, else
           // advance input_open port only
           if (gate_flow == true){
               output.sample().data().resize(input_length);
               for (int i = 0; i < length_in_bits; i++){
                  out_data_ptr[i] = in_data_ptr[i];
               }
               output.advance();
           }
           input_open.advance();
       }
       // Finished going through each sample, get new input message
       else if (input_open.opCode() == Bool_timed_sampleTime_OPERATION)
       {
          // Pass through time opcode and time data
          output.setOpCode(Bool_timed_sampleTime_OPERATION);
          output.time().fraction() = input_open.time().fraction();
          output.time().seconds() = input_open.time().seconds();
	  return RCC_ADVANCE;
       } 
       else if (input_open.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
          // Pass through sample interval opcode and sample interval data
          output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
          output.sample_interval().fraction() =
          input_open.sample_interval().fraction();
          output.sample_interval().seconds() =
          input_open.sample_interval().seconds();
          return RCC_ADVANCE;
       } 
       else if (input_open.opCode() == Bool_timed_sampleFlush_OPERATION) {
	  // Pass through flush opcode and reset shift register
          output.setOpCode(Bool_timed_sampleFlush_OPERATION);
          return RCC_ADVANCE;
       } 
       else if (input_open.opCode() == Bool_timed_sampleMetadata_OPERATION) {
          // Pass through metadata opcode, id, and data
          output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
          output.metadata().id() = input_open.metadata().id();
          output.metadata().value() = input_open.metadata().value();
          return RCC_ADVANCE;
       } 
       else {
           setError("Unknown OpCode received");
           return RCC_FATAL;
       }
        /* Process INPUT_CLOSE port */
	// Identify received Discontinity opcode on input_close port as
	// indicator to not allow samples to flow from input_open port
	// to output port. Advance input_close only port on any received
	// opcode.
        if (input_close.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
           gate_flow = false;  //toggle gate to closed
           printf("discontinuity_gate_b : CLOSED\n");
           input_close.advance();
           done = true;
	   return RCC_OK;
        }
        else if (input_close.opCode() == Bool_timed_sampleSample_OPERATION){
           input_close.advance();
        } 
	else if (input_close.opCode() == Bool_timed_sampleTime_OPERATION) {
           input_close.advance();
        } 
	else if (input_close.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
           input_close.advance();
        } 
	else if (input_close.opCode() == Bool_timed_sampleFlush_OPERATION) {
           input_close.advance();
        } 
	else if (input_close.opCode() == Bool_timed_sampleMetadata_OPERATION) {
           input_close.advance();
        } 
	else {
           setError("Unknown OpCode received");
           return RCC_FATAL;
        }

   // Setting DONE state supports FSK demonstration application.  Used to 
   // identify when packet has been fully received and app can close.
   if (done == true){
      if (count == 1) { //must wait for unit test
          return RCC_DONE;
      }
      count++;
   }
   return RCC_OK;
  }
};

DISCONTINUITY_GATE_B_START_INFO
DISCONTINUITY_GATE_B_END_INFO

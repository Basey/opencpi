/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "drc_fmcomms-worker.hh"
#include <cmath>

// These are the helper classes for the ad9361 helpers
#include "RadioCtrlrConfiguratorTuneResamp.hh"
#include "RadioCtrlrConfiguratorAD9361.hh"
#include "RadioCtrlrNoOSTuneResamp.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Drc_fmcommsWorkerTypes;

//This is for the generic drc helper class
#include "OcpiDrcProxyApi.hh"

namespace OD = OCPI::DRC;

double rx_sampling_rate_Msps = 0.0;
double tx_sampling_rate_Msps = 0.0;
double rx_offset_freq_MHz = 0.0;
double tx_offset_freq_MHz = 0.0;

//Value set based on minimum clock required by interface
static double min_ad9361_clock_rate_Msps = 40.0;

class Drc_fmcommsWorker : public OD::DrcProxyBase {

  // ===========================================================================
  // To use the ad9361 DRC helper classes, we need a configurator class that 
  // combines the bas ad9361 one with the tuneresamp soft tuning
  class Fsk_Configurator: public OD::ConfiguratorAD9361, 
                          public OD::ConfiguratorTuneResamp {
  public:
    Fsk_Configurator()
      : OD::ConfiguratorAD9361(DRC_FMCOMMS_RF_PORTS_RX.data[0], NULL,
                               DRC_FMCOMMS_RF_PORTS_TX.data[0], NULL), 
        OD::ConfiguratorTuneResamp(ad9361MaxRxSampleMhz(),
                                   ad9361MaxTxSampleMhz()) { 
    }
    // All concrete Configurator classes must have this clone method for
    // virtual copying. 
    OD::Configurator *clone() const { return new Fsk_Configurator(*this); }
  protected:
    // The virtual callback to impose all constraints. 
    void impose_constraints_single_pass() {
      ConfiguratorAD9361::impose_constraints_single_pass();
      ConfiguratorTuneResamp::impose_constraints_single_pass();
      Configurator::impose_constraints_single_pass();
    }
  } m_configurator;

  // ===========================================================================
  // trampoline between the DRC ad9361 helper classes in the framework library 
  // and the slave workers accessible from this worker.  It lets the former call
  // the latter
  struct DoSlave : OD::DeviceCallBack {
    Slaves &m_slaves;
    DoSlave(Slaves &slaves) : m_slaves(slaves) {}
    void get_byte(uint8_t /*id_no*/, uint16_t addr, uint8_t *buf) {
      m_slaves.config.getRawPropertyBytes(addr, buf, 1);
    }
    void set_byte(uint8_t /*id_no*/, uint16_t addr, const uint8_t *buf) {
      m_slaves.config.setRawPropertyBytes(addr, buf, 1);
    }
    void set_reset(uint8_t /*id_no*/, bool on) {
      m_slaves.config.set_force_reset(on ? 1 : 0);
    }
    bool isMixerPresent(bool rx, unsigned stream) {
      return false;
    }
    OD::config_value_t getDecimation(unsigned /*stream*/) {
     #if OCPI_PARAM_drc_fmcomms_enable_rx()
        int down_samp_factor = 1;// Default decimation factor
        int cic_scale = 1;// Default scale factor for resample factor equal to 1
        int cic_delay = m_slaves.rx_cic.get_cic_differential_delay();
        int cic_order = m_slaves.rx_cic.get_cic_order();

        // Calculate decimation factor if sample rate less than minimum rate    
        if (rx_sampling_rate_Msps < min_ad9361_clock_rate_Msps){
                down_samp_factor = ceil(min_ad9361_clock_rate_Msps
                                   / rx_sampling_rate_Msps);
        }
        // Calculate decimtor gain
        int cic_gain = pow((down_samp_factor*cic_delay),cic_order);
        // Change scale factor if down sample factor not equal to 1
	// Typical scaling factor per component doc
        if (down_samp_factor > 1) cic_scale = ceil(log2(cic_gain));
        
        m_slaves.rx_cic.set_scale_output(cic_scale);
        m_slaves.rx_cic.set_down_sample_factor(down_samp_factor);

        //Set RX carrier offset
        if (rx_offset_freq_MHz != 0.0) {
                double step_size = rx_offset_freq_MHz * pow(2,32)
                                   / (rx_sampling_rate_Msps * down_samp_factor);
                m_slaves.rx_offset_const.set_value(step_size);
        }

      return m_slaves.rx_cic.isPresent() ? 
             m_slaves.rx_cic.get_down_sample_factor() : 1;
    #else
      return 1;
    #endif
    }
    OD::config_value_t getInterpolation(unsigned /*stream*/) {
      #if OCPI_PARAM_drc_fmcomms_enable_tx()
        int up_samp_factor = 1;// Default interpolation factor
        int cic_scale = 1;// Default scale factor for resample factor equal to 1
        int cic_delay = m_slaves.tx_cic.get_cic_differential_delay();
        int cic_order = m_slaves.tx_cic.get_cic_order();

        // Calculate interpolation factor if sample rate less than minimum rate
        if (tx_sampling_rate_Msps < min_ad9361_clock_rate_Msps){
                up_samp_factor = ceil(min_ad9361_clock_rate_Msps
                                 / tx_sampling_rate_Msps);
        }
        // Calculate interpolation gain
        int cic_gain = pow((up_samp_factor*cic_delay),cic_order)/up_samp_factor;
        // Change scale factor if up sample factor not equal to 1
        if (up_samp_factor > 1) cic_scale = ceil(log2(cic_gain));

        m_slaves.tx_cic.set_scale_output(cic_scale);
        m_slaves.tx_cic.set_up_sample_factor(up_samp_factor);

	//Set transmit carrier offset
        if (tx_offset_freq_MHz != 0.0) {
                double step_size = tx_offset_freq_MHz * pow(2,32)
                                  / (tx_sampling_rate_Msps * up_samp_factor);
    		m_slaves.tx_offset_const.set_value(step_size);
        }

      return m_slaves.tx_cic.isPresent() ?
             m_slaves.tx_cic.get_up_sample_factor() : 1;
    #else
      return 1;
    #endif
    }
    OD::config_value_t getPhaseIncrement(bool rx, unsigned /*stream*/) {
      return 0;
    }
    void setPhaseIncrement(bool rx, unsigned /*stream*/, int16_t inc) {
      return;
    }
    void initialConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      OD::ad9361InitialConfig(m_slaves.config, m_slaves.data_sub, config);
      config.xo_disable_use_ext_ref_clk=false;
    }
    void postConfig(uint8_t /*id_no*/) {
      OD::ad9361PostConfig(m_slaves.config);
    }
    void finalConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      OD::ad9361FinalConfig(m_slaves.config, config);
    }
    // both of these apply to both channels on the 9361
    unsigned getRfInput(unsigned /*device*/, OD::config_value_t ) { return 0; }
    unsigned getRfOutput(unsigned /*device*/, OD::config_value_t ) { return 0; }
  } m_doSlave;

  OD::RadioCtrlrNoOSTuneResamp m_ctrlr;
  OD::ConfigLockRequest m_requests[DRC_FMCOMMS_MAX_CONFIGURATIONS_P];

public:
  Drc_fmcommsWorker()
    : m_doSlave(slaves),
      m_ctrlr(0, "drc_fmcomms", m_configurator, m_doSlave) {
  }
  // ===========================================================================
  // These methods interface with the helper 9361 classes etc.
  // ===========================================================================
  RCCResult prepare_config(unsigned config) {
    printf("drc : fmcomms\n");
    log(8, "DRC: prepare_config: %u", config);

    auto &conf = m_properties.configurations.data[config];
    auto &req = m_requests[config];
    // So here we basically convert the data structure dictated by the drc spec
    // property to the one defined by the older DRC/ad9361 helper classes
    auto nChannels = conf.channels.length;
    req.m_data_streams.resize(nChannels);
    unsigned nRx = 0, nTx = 0;
    for (unsigned n = 0; n < nChannels; ++n) {
      auto &channel = conf.channels.data[n];
      auto &stream = req.m_data_streams[n];
      /*
      printf("   channel: rx = %d\n",channel.rx);
      printf("   tuning_freq = %0.3f\n",channel.tuning_freq_MHz);
      printf("   sample_rate = %0.3f MSps\n",channel.sampling_rate_Msps);
      printf("   bandwidth   = %0.3f MHz\n",channel.bandwidth_3dB_MHz);
      printf("   offset_freq = %0.3f MHz\n",channel.offset_freq_MHz);
      */
      if (channel.rx == 1){
        rx_sampling_rate_Msps = channel.sampling_rate_Msps;
	rx_offset_freq_MHz = channel.offset_freq_MHz;
      }
      if (channel.rx == 0){
        tx_sampling_rate_Msps = channel.sampling_rate_Msps;
	tx_offset_freq_MHz = channel.offset_freq_MHz;
      }

      stream.include_data_stream_type(channel.rx ?
                       OD::data_stream_type_t::RX : OD::data_stream_type_t::TX);
      stream.include_data_stream_ID(channel.rx ?
                                    DRC_FMCOMMS_RF_PORTS_RX.data[nRx] :
                                    DRC_FMCOMMS_RF_PORTS_TX.data[nTx]);
      stream.include_routing_ID((channel.rx ? "RX" : "TX") +
                                std::to_string(channel.rx ? nRx : nTx));
      ++(channel.rx ? nRx : nTx);
      stream.include_tuning_freq_MHz(channel.tuning_freq_MHz,
                                     channel.tolerance_tuning_freq_MHz);
      stream.include_bandwidth_3dB_MHz(channel.bandwidth_3dB_MHz,
                                       channel.tolerance_bandwidth_3dB_MHz);
      stream.include_sampling_rate_Msps(channel.sampling_rate_Msps,
                                        channel.tolerance_sampling_rate_Msps);
      stream.include_samples_are_complex(channel.samples_are_complex);
      stream.include_gain_mode(channel.gain_mode);
      if (!stream.get_gain_mode().compare("manual"))
        stream.include_gain_dB(channel.gain_dB, channel.tolerance_gain_dB);
    }
    // Ideally we would validate them here, but not now.
    return RCC_OK;
  }
  RCCResult start_config(unsigned config) {
    try {
      return m_ctrlr.request_config_lock(std::to_string(config),
                                         m_requests[config]) ?
        RCC_OK :
        setError("config lock request was unsuccessful, set OCPI_LOG_LEVEL to 8"
                 " (or higher) for more info");
    } catch(const char* err) {
      return setError(err);
    }
    return RCC_OK;
  }
  RCCResult stop_config(unsigned config) { 
    log(8, "DRC: stop_config: %u", config);
    
    // Unlock the configurations when stopping the DRC.
    m_ctrlr.unlock_all();    
    log(8, "DRC: stop_config -> Issued unlock for all configurations"); 

    return RCC_OK;
  }
  RCCResult release_config(unsigned /*config*/) {
    log(8, "DRC: release_config");
    return m_ctrlr.shutdown() ?
      setError("transceiver shutdown was unsuccessful, set OCPI_LOG_LEVEL to 8"
               " (or higher) for more info") : RCC_OK;
  }
};

DRC_FMCOMMS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DRC_FMCOMMS_END_INFO

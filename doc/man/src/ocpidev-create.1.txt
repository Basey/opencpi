OCPIDEV-CREATE(1)
=================

NAME
----
ocpidev-create - create OpenCPI assets


SYNOPSIS
--------
*`ocpidev`* ['<options>'] *`create`* '<noun>' '<name>'


DESCRIPTION
-----------
The *`create`* verb creates, in a project, the OpenCPI asset
specified by the '<noun>' and '<name>' arguments that follow the verb.

Asset types to be specified in the '<noun>' argument are:

*`application`*::
    Create an application: XML or ACI as specified by options.

*`component`*::
    Create a component in a component library, or, based on options, at the top level of a project.

*`hdl`*::
    A prefix to indicate an HDL asset in the *`hdl/`* subdirectory of a project.
    Possible HDL assets are:
    
        *`assembly`*;;
	Create an HDL assembly.
	
	*`card`*;;
	Create an HDL card definition.
		
	*`device`*;;
	Create an HDL device worker, possibly specific
	to a platform.
	
	*`platform`*;;
	Create an HDL platform.
	
	*`primitive`*;;
	Create an HDL primitive. Possible primitive types are:
	
	    *`core`*:::
	    A single primitive module, perhaps presynthesized
	    (for example, from Xilinx Coregen or Altera Megawizard)

	    *`library`*:::
	    A set of HDL primitive modules to use from workers or other primitive cores or libraries.
	    
	*`slot`*;;
	Create an HDL slot definition.
	
*`library`*::
    Create a component library.  

*`project`*::
    Create a project which holds and organizes OpenCPI assets.

*`protocol`*::
    Create a protocol in a library or project.
    
*`registry`*::
    Create a registry that enables projects to depend on each other.

*`test`*::
    Create a component unit test suite in a library.

*`worker`*::
    Create a worker in a library based on a component specification.
    
All HDL assets should avoid having names that are Verilog or VHDL-
reserved identifiers, such as reg, etc.

Note that the *`create`* verb creates skeleton OpenCPI asset document files
for most of the buildable OpenCPI assets when it creates the asset.
See the link:ocpidev-build.1.html[ocpidev-build(1)] man page and the
"noun" man pages for individual assets for details.

OPTIONS
-------
In addition to the general-purpose options common
to all OpenCPI tools (see link:opencpi.1.html[opencpi(1)]),
the options described below can be specified for the *`create`* verb.
These options are usually specific to the asset type
that is being created. If creation fails, all partially-created
directories and/or files are removed unless the *`-k`* (*`--keep`*)
option is specified.

Many options to the *`create`* verb set initial values in the asset's
XML description file that can subsequently be edited for any required
changes or additions.

In the following descriptions, the plus sign (+) after the option
indicates that it can be specified more than once on the command line.

Option When Creating Any Asset Type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
*`-k`*, *`--keep`*::
    Keep files and directories created after an asset creation
    fails. Normally, all such files and directories are removed
    on any failure.

Options When Creating Projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-D`* '<package-ID>', *`--depend`* '<package-ID>'+::
    Specify the package-ID of a project on which the project being
    created depends.

*`--register`*::
   Register the project as part of creating it.

Options When Creating Projects or Libraries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-F`* '<package-prefix>', *`--package-prefix=`*'<package-ID>'::
   Specify the package prefix of the created project or library.
   The default is *local* for projects and '<package-ID of parent>'
   for libraries.

*`-K`* '<package-ID>', *`--package-id=`*'<package-ID>'::
   Specify the package-ID directly. The default is
   '<package-prefix>'*.*'<package-name>'.

*`-N`* '<package-name>', *`--package-name=`*'<package-name>'::
   Specify the package name of the created project or library. The
   default is the '<name>' argument given after the *create* verb.

Options When Creating Projects, Libraries, Workers, HDL Devices or HDL Platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-A`* '<directory>', *`--xml-include=`*'<directory>'+::
   Specify a directory to search for XML include files.

*`-I`* '<directory>', *`--include-dir=`*'<directory>'+::
   Specify a directory to search for include files (C, C++,
   Verilog).

*`-Y`* '<primitive-library>', *`--primitive-library=`*'<primitive-library>'+::
   Specify a primitive library on which the asset (or assets
   it contains) depends.

*`-y`* '<component-library>', *`--component-library=`*'<component-library>'+::
   Specify a component library to search for workers, devices
   and/or specs that this asset (or assets it contains) references.
   
Options When Creating Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-n`*, *`--no-control`*::
   Specify that the component has no control interface. This option
   is rarely used or recommended, but is required for certain
   infrastructure components. It applies only to components
   with HDL-only implementations.

*`-t`*, *`--create-test`*::
   Create the component unit test suite for the component as part of creating the
   component in a component library.
   
Option When Creating Components (Specs) or Protocols
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-p`*, *`--project`*::
   Create the component specification or protocol in the *`specs/`*
   directory of the project. No asset directories or document files are created.

Options When Creating Protocols, Workers or HDL Devices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-h`* '<hdl-library>', *`--hdl-library=`*'<hdl-library>'::
   Create the asset in the component library under the
   *`hdl/`* directory of the project. Valid values are only
   one of *devices*, *cards* or *adapters*.

*`-l`* '<library>', *`--library=`*'<library>'::
   Create the asset in the specified component library.

Option When Creating Workers (All Types) and Tests
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-S`* '<spec>', *`--component`*::
   Specify the component (aka "spec") to be
   implemented by the worker/test. By default, the *`ocpidev create`* operation
   searches for a component spec whose name matches
   the name of the worker/test being created.  Use the *`-S`*
   option to specify a different component spec name to search for.
   The operation searches for a component spec in the component
   directory in the same library as the worker/test, in the *`specs/`* directory
   in the same library as the worker/test (for backward compatibility with
   earlier OpenCPI releases), in the top-level *`specs/`* directory in the
   project, and in other projects, if the *`--depend`* (*`-D`*) option
   was specified when the project containing the worker/test was created
   or dependencies are specified in the *`Project.xml`* file.

Options When Creating Workers (All Types)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-L`* '<language>', *`--language=`*'<language>'::
   Specify the source language for the worker being created.
   The default is the default language for the authoring
   model. It must be C or C++ for RCC workers and VHDL for
   HDL workers.

*-`O`* '<file>', *`--other=`*'<file>'+::
   Specify a source code file that is not included by default
   (that is, in addition to the '<worker>'*.*[*c*|*cc*|*vhd*] file)
   to be compiled when building the worker.

*-`P`* '<platform>', *`--platform=`*'<platform>'::
   Create the worker in the *devices* library for the specified
   HDL platform (in this project).

Options When Creating RCC Workers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-R`* '<prereq-library>', *`--static-prereq=`*'<prereq-library>'+::
   Specify a prerequisite library on which the worker being created
   depends and to which it should be statically linked.
   
*`-V`* '<slave-worker>', *`--slave-worker=`*'<slave-worker>'+::
   Create the RCC worker as a proxy for the specified slave
   worker.

*`-W`* '<worker>', *`--worker=`*'<worker>'+::
   Specify one of multiple workers implemented in the RCC
   worker's directory when a single RCC worker directory
   is creating a multi-worker artifact. This option is
   supported but is rarely required or recommended.

*`-r`* '<prereq-library>', *`--rcc-dynamic=`*'<prereq-library>'+::
   Specify a library on which the worker being created
   depends and to which it should be dynamically linked.

Option When Creating HDL Workers, Devices and Platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-C`* '<core>', *`--core=`*'<core>'+::
   Specify an HDL primitive core on which the worker being
   created depends and with which it should be built.

Options When Creating HDL Devices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-E`* '<hdl-device>', *`--emulates=`*'<hdl-device>'+::
   Create the worker as an emulator worker and specify the
   HDL device worker associated with the device being emulated.

*`-U`* '<hdl-device>', *`--supports=`*'<hdl-device>'+::
   Create the worker as a subdevice and specify one of the
   HDL device workers that the subdevice supports.

Options When Creating HDL Platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-g`* '<part>', *`--part=`*'<part>'::
   Specify the part (die-speed-package;, e.g., *xc7z020-1-clg484*)
   for the HDL platform being created.

*`-q`* '<frequency>', *`--time-freq=`*'<frequency>'::
   Specify the time server frequency for the platform.

Options When Creating Any Buildable Asset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-Q`* '<platform>', *`--exclude-platform=`*'<platform>'+::
   Do not build the asset for the specified platform.

*`-G`* '<platform>', *`--only-platform=`*'<platform>'+::
   Only build the asset for the specified platform(s).

Options When Creating Any Buildable HDL Asset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-T`* '<target>', *`--only-target=`*'<target>'+::
   Only build the asset for the specified HDL architecture or part family.
   
*`-Z`* '<target>', *`--exclude-target=`*'<target>'+::
   Do not build the asset for the specified HDL architecture or part family.

Options When Creating HDL Primitive Libraries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-H`*, *`--no-depend`*::
   Specify that the HDL primitive library being created does not
   depend on any other primitive library. By default, HDL primitive
   libraries are assumed to depend on certain built-in primitive
   libraries in *`$OCPI_ROOT_DIR/projects/core/hdl/primitives`*
   (currently, *`bsv`*, *`cdc`*, *`fixed_float`*, and *`ocpi`*).

*`-J`*, *`--no-elaborate`*::
   Specify that the HDL primitive library being created should not
   be elaborated when building, which results in a slight time
   savings at the cost of catching elaboration errors earlier in
   the build process.

Options When Creating HDL Primitive Cores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-B`* '<file>', *`--prebuilt=`*'<file>'::
   Create the HDL primitive core from the specified pre-built
   core file (e.g., an *`.edf`* file from Xilinx Vivado,
   an *`.ngc`* file from Coregen or a *`.qxp`* file from Quartus).
   
*`-M`* '<module>', *`--module=`*'<module>'::
   Specify the name of the top module of the core. The default is
   the name of the core specified in the '<name>' argument after
   *`create`*.

Options When Creating Applications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-X`*, *`--xml-app`*::
   Create the application as an XML file in the *`applications/`* directory
   of the project.

*`-x`*, *`--xml-dir-app`*::
   Specify that the application has its own directory but it will
   only be an XML file. The default creates an application with
   an ACI C++ file. This option cannot be used with the *`-X`* option.

EXAMPLES
--------
. Create in the current working directory a project with the name *`my-project`*. This project's
package-ID will be *`local.my-project`*:
+
---------------------------------
ocpidev create project my-project
---------------------------------
+
. Create in the current working directory a project with
the package-prefix *`org.my_organization`* and
name *`my_project`*. This project's package-ID will be
*`org.my_organization.my_project`*.
+
--------------------------------------------------------
ocpidev create project my-project -F org.my_organization
--------------------------------------------------------
+
. Create a project registry *`/home/workspace/my-registry`*. Note that a
new registry will not have a core project registered, so the next
thing to do is locate and register a core project. Finally, use
*`ocpidev set registry /home/workspace/my-registry`* from within a project
to tell that project to use the new registry. None of this is
necessary if you are using the default project registry in the
OpenCPI installation.
+
------------------------------------------------------
ocpidev -d /home/workspace create registry my-registry
------------------------------------------------------
+
. Create the default *`components`* library for a project. If you
provide a name other than *`components`*, *`ocpidev`* creates it
underneath the *`components`* directory.
+
---------------------------------
ocpidev create library components
---------------------------------
+
. Create a skeleton component specification and component
asset document for a component named *`mycomp`*. The files
are created in the *`mycomp.comp/`* directory in
the *`components`* library. 
Use the *`-l`* '<library-name>' (*`--library=`*'<library-name>')
option if there are sublibraries underneath *`components`*.
+
-------------------------------
ocpidev create component mycomp
-------------------------------
+
. Create the skeleton files and worker asset document
for an HDL worker named *`fft_fast`* that implements the
*`fft`* component spec located in the component directory
*`fft.comp/fft-comp.xml`* in the same library as the worker.
+
-----------------------------------------
ocpidev create worker fft_fast.hdl -S fft
-----------------------------------------
+
. Create the skeleton files and asset document
for an HDL assembly named *`MyPlutoAsm`* to be
used only on the *`plutosdr`* HDL platform.
+
------------------------------------------------
ocpidev create hdl assembly MyPlutoAsm -G plutosdr
------------------------------------------------

NOTES
-----
Component specifications created in earlier OpenCPI releases
reside in a *`specs/`* directory located either in a component library
or at the top level of a project and are named
'<name>'*`-spec.xml`* or '<name>'*`_spec.xml`*.

The following option to the *`ocpidev create`* operation for a component is
provided for backward compatibility with the component spec structure used
in earlier OpenCPI releases:

*`ocpidev create component`* '<name>' *`--file-only`*

The *`--file-only`* option directs *`ocpidev`* to create a component spec named
'<name>'*`-spec.xml`* in a *`specs/`* directory in a component library.
No component asset document is created.

BUGS
----
See https://www.opencpi.org/report-defects


RESOURCES
---------
See the main web site: https://www.opencpi.org

See the 'OpenCPI Application Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Application_Development_Guide.pdf

See the 'OpenCPI Component Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf

See the 'OpenCPI HDL Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_HDL_Development_Guide.pdf

See the 'OpenCPI Platform Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf

See the 'OpenCPI RCC Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_RCC_Development_Guide.pdf

See the 'OpenCPI Documentation Writer Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Documentation_Writer_Guide.pdf


SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-build.1.html[ocpidev-build(1)]
link:ocpidev-clean.1.html[ocpidev-clean(1)]
link:ocpidev-delete.1.html[ocpidev-delete(1)]
link:ocpidev-refresh.1.html[ocpidev-refresh(1)]
link:ocpidev-register.1.html[ocpidev-register(1)]
link:ocpidev-run.1.html[ocpidev-run(1)]
link:ocpidev-set.1.html[ocpidev-set(1)]
link:ocpidev-show.1.html[ocpidev-show(1)]
link:ocpidev-unregister.1.html[ocpidev-unregister(1)]
link:ocpidev-unset.1.html[ocpidev-unset(1)]
link:ocpidev-utilization.1.html[ocpidev-utilization(1)]


COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

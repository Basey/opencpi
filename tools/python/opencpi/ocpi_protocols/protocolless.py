#!/usr/bin/env python3

# Helper functions for protocol-less components
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Common functions for protocol-less components."""

from collections import namedtuple
import opencpi.ocpi_testing.ocpi_testing.generator as generator

SampleTraits = namedtuple(
    "SampleTraits", ["type_name", "generator", "block_generator"])
timed_sample_protocol_trait_table = {
    (8, False, False): SampleTraits(
        "uchar_timed_sample",
        generator.UnsignedCharacterGenerator,
        generator.UnsignedCharacterBlockGenerator),
    (8, True, False): SampleTraits(
        "char_timed_sample",
        generator.CharacterGenerator,
        generator.CharacterBlockGenerator),
    (8, True, True): SampleTraits(
        "complex_char_timed_sample",
        generator.ComplexCharacterGenerator,
        generator.ComplexCharacterBlockGenerator),
    (16, False, False): SampleTraits(
        "ushort_timed_sample",
        generator.UnsignedShortGenerator,
        generator.UnsignedShortBlockGenerator),
    (16, True, False): SampleTraits(
        "short_timed_sample",
        generator.ShortGenerator,
        generator.ShortBlockGenerator),
    (16, True, True): SampleTraits(
        "complex_short_timed_sample",
        generator.ComplexShortGenerator,
        generator.ComplexShortBlockGenerator),
    (32, False, False): SampleTraits(
        "ulong_timed_sample",
        generator.UnsignedLongGenerator,
        generator.UnsignedLongBlockGenerator),
    (32, True, False): SampleTraits(
        "long_timed_sample",
        generator.LongGenerator,
        generator.LongBlockGenerator),
    (32, True, True): SampleTraits(
        "complex_long_timed_sample",
        generator.ComplexLongGenerator,
        generator.ComplexLongBlockGenerator),
    (64, False, False): SampleTraits(
        "ulonglong_timed_sample",
        generator.UnsignedLongLongGenerator,
        generator.UnsignedLongLongBlockGenerator),
    (64, True, False): SampleTraits(
        "longlong_timed_sample",
        generator.LongLongGenerator,
        generator.LongLongBlockGenerator),
    # Complex 64 bit does not have a generator, but can still be used as an output type.
    (64, True, True): SampleTraits(
        "complex_longlong_timed_sample",
        None,
        None),
}


class timed_sample_protocol_traits:
    """Integer type timed sample protocol attributes."""

    def __init__(self, width, signed_type=True, complex_type=False):
        """Initialise class.

        Args:
            width (int): Integer width 8,16,32,or 64.
            signed_type (bool): Is a signed type.
            complex_type (bool): Is a complex type.
        """
        key = (width, signed_type, complex_type)
        if key in timed_sample_protocol_trait_table:
            self.traits = timed_sample_protocol_trait_table[(
                width, signed_type, complex_type)]
        else:
            raise ValueError(
                f"Unsupported protocol type. width: {width} signed: {signed_type} complex: {complex_type}")
        self.name = self.traits.type_name
        self.generator = self.traits.generator
        self.block_generator = self.traits.block_generator
        self.width_bits = width
        self.width_bytes = width // 8
        if signed_type:
            self.max_value = 2**(width-1)-1
            self.min_value = -2**(width-1)
        else:
            self.max_value = 2**width - 1
            self.min_value = 0

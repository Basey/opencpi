#!/usr/bin/env python3

# Testing of code in base_generator.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Pytest for Base Block Generator."""

import numpy
import random
import statistics
import unittest

from opencpi.ocpi_testing.ocpi_testing.generator.base_block_generator import BaseBlockGenerator


class BaseBlockGeneratorTest(BaseBlockGenerator):
    """BaseBlockGenerator class for testing."""

    def _generate_random_samples(self, number_of_samples):
        """Override of base class.

        The Base Block Generator does not implement this method, but it is used
        by other base class methods. This adds a 'test' implementation to allow
        more comprehensive test coverage of the base class.
        The range is set to +/-2^64 which is larger than the sine wave generator
        so the type of generated samples can be detected.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        data = [0.0] * number_of_samples
        for index in range(number_of_samples):
            data[index] = random.uniform(-2.0**64,
                                         2.0**64)
        return data

    def _full_scale_random_sample_values(self, number_of_samples):
        """Required for soak case test.

        The data is not important, just that samples are produced.
        """
        return self._generate_random_samples(number_of_samples)


class TestBaseBlockGenerator(unittest.TestCase):
    """pytest for Base Block Generator"""

    def setUp(self):
        self.test_generator = BaseBlockGeneratorTest(
            block_length=48, number_of_soak_blocks=10)
        # Base Generator does not contain protocol specific values. Set up
        # some values required for base class testing.
        TEST_MAXIMUM = 2.0**15
        self.test_generator.TYPICAL_AMPLITUDE_MEAN = TEST_MAXIMUM / 9
        self.test_generator.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH = TEST_MAXIMUM / 42
        self.test_generator.TYPICAL_MAXIMUM_AMPLITUDE = TEST_MAXIMUM * \
            self.test_generator.LIMITED_SCALE_FACTOR
        self.test_generator.MESSAGE_SIZE_LONGEST = 128
        self.seed = 42

    def _analyse_messages(self, messages):
        """Analyse messages from the generator.

        Counts the number of each type of message opcode so that it is easy to
        test for the existence and non-existence of opcodes in the results.
        For sample opcodes calculates the minimum and maximum values so that
        the range of generated values can be checked.
        """
        count = 0
        minimum = 2**64
        maximum = -2**64
        opcodes = {"sample": 0, "time": 0, "sample_interval": 0,
                   "flush": 0, "discontinuity": 0, "metadata": 0, "test": 0}
        for m in messages:
            self.assertTrue(m["opcode"] in opcodes)
            opcodes[m["opcode"]] += 1
            if m["opcode"] == "sample":
                self.assertGreater(len(m["data"]), 0)
                count += len(m["data"])
                minimum = min(minimum, min(m["data"]))
                maximum = max(maximum, max(m["data"]))
        return count, minimum, maximum, opcodes

    def test_get_wave_parameters(self):
        (frequencies, phases, amplitudes) = self.test_generator._get_wave_parameters()
        self.assertNotEqual(len(frequencies), 0)
        self.assertEqual(len(frequencies), len(phases))
        self.assertEqual(len(frequencies), len(amplitudes))
        for f in frequencies:
            self.assertGreater(f, 0)
        for p in phases:
            self.assertGreater(p, 0)

    def test_generate_sine_wave_samples(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._generate_sine_wave_samples(n)

        self.assertGreater(len(data), self.test_generator._block_length)
        mean = statistics.mean(data)
        self.assertLess(abs(mean), self.test_generator.TYPICAL_AMPLITUDE_MEAN)

    def test_get_sample_values(self):
        # Default length
        data = self.test_generator._get_sample_values()
        self.assertEqual(len(data), self.test_generator.SAMPLE_DATA_LENGTH)
        # Given length
        data = self.test_generator._get_sample_values(1)
        self.assertEqual(len(data), 1)
        data = self.test_generator._get_sample_values(89071)
        self.assertEqual(len(data), 89071)

    def test_generate_sample_opcodes(self):
        # Test minimum generation.
        messages = self.test_generator._generate_sample_opcodes(0)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]), 1)
        # Test single message.
        messages = self.test_generator._generate_sample_opcodes(13)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]), 13)
        # Test multiple messages.
        messages = self.test_generator._generate_sample_opcodes(
            self.test_generator.MESSAGE_SIZE_LONGEST*13+27)
        self.assertEqual(len(messages), 14)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(messages[1]["opcode"], "sample")
        self.assertEqual(messages[13]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        self.assertEqual(len(messages[12]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        self.assertEqual(len(messages[13]["data"]), 27)

    def test_split_samples_into_opcodes(self):
        # Single sample -> single sample opcode
        data = self.test_generator._generate_sine_wave_samples(1)
        messages = self.test_generator._split_samples_into_opcodes(data)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]), 1)
        # Maximum message size samples -> single sample opcode
        data = self.test_generator._generate_sine_wave_samples(
            self.test_generator.MESSAGE_SIZE_LONGEST)
        messages = self.test_generator._split_samples_into_opcodes(data)
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        # One more than maximum -> 2 opcodes (max size, 1 sample)
        data = self.test_generator._generate_sine_wave_samples(
            self.test_generator.MESSAGE_SIZE_LONGEST+1)
        messages = self.test_generator._split_samples_into_opcodes(data)
        self.assertEqual(len(messages), 2)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(messages[1]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        self.assertEqual(len(messages[1]["data"]), 1)
        # One less than multiple of max - last message has one less than maximum.
        data = self.test_generator._generate_sine_wave_samples(
            self.test_generator.MESSAGE_SIZE_LONGEST*3-1)
        messages = self.test_generator._split_samples_into_opcodes(data)
        self.assertEqual(len(messages), 3)
        self.assertEqual(messages[0]["opcode"], "sample")
        self.assertEqual(messages[1]["opcode"], "sample")
        self.assertEqual(messages[2]["opcode"], "sample")
        self.assertEqual(len(messages[0]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        self.assertEqual(len(messages[1]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST)
        self.assertEqual(len(messages[2]["data"]),
                         self.test_generator.MESSAGE_SIZE_LONGEST-1)

    def test_typical(self):
        messages = self.test_generator.typical(self.seed, "none")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Uses sine wave so check limits
        self.assertLess(maximum, 2**16)
        self.assertGreater(minimum, -2**16)

    def test_property(self):
        messages = self.test_generator.property(self.seed, "none")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertEqual(count, self.test_generator._block_length)
        # Uses random
        self.assertGreater(maximum, 2**16)
        self.assertLess(minimum, -2**16)

    def test_input_stressing(self):
        messages = self.test_generator.input_stressing(self.seed, "none")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(
            count, 2*self.test_generator._block_length)
        self.assertGreaterEqual(
            count, 2*self.test_generator.SAMPLE_DATA_LENGTH)

    def test_sample(self):
        messages = self.test_generator.sample(self.seed, "random")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(
            count, self.test_generator._block_length)
        self.assertGreaterEqual(
            count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Check random values
        self.assertGreater(maximum, 2**16)
        self.assertLess(minimum, -2**16)

        messages = self.test_generator.sample(self.seed, "sinusoidal")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Check random values
        self.assertLess(maximum, 2**16)
        self.assertGreater(minimum, -2**16)

    def test_message_size(self):
        messages = self.test_generator.message_size(self.seed, "shortest")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        for m in messages:
            self.assertEqual(
                len(m["data"]), self.test_generator.MESSAGE_SIZE_SHORTEST)

        messages = self.test_generator.message_size(self.seed, "longest")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        for m in messages:
            self.assertEqual(
                len(m["data"]), self.test_generator.MESSAGE_SIZE_LONGEST)

        messages = self.test_generator.message_size(
            self.seed, "different_sizes")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        minimum = self.test_generator.MESSAGE_SIZE_LONGEST
        maximum = 0
        for m in messages:
            minimum = min(minimum, len(m["data"]))
            maximum = max(maximum, len(m["data"]))
        self.assertGreaterEqual(
            minimum, self.test_generator.MESSAGE_SIZE_SHORTEST)
        self.assertLessEqual(maximum, self.test_generator.SAMPLE_DATA_LENGTH)
        self.assertNotEqual(minimum, maximum)

    def test_inter_intra_tests(self):
        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "single", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Minimum number of messages.
        self.assertGreaterEqual(len(messages), 3)
        # Only sample and test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        count = 0
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "sample":
                count += len(messages[m]["data"])
            if messages[m]["opcode"] == "test":
                # Check an exact block length
                self.assertEqual(count % self.test_generator._block_length, 0)

        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "intra_single", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Minimum number of messages.
        self.assertGreaterEqual(len(messages), 3)
        # Only sample and test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        count = 0
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "sample":
                count += len(messages[m]["data"])
            if messages[m]["opcode"] == "test":
                # Check not an exact block length
                self.assertNotEqual(
                    count % self.test_generator._block_length, 0)

        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "consecutive", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Minimum number of messages.
        self.assertGreaterEqual(len(messages), 3)
        # Only sample and at least 2 test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        self.assertGreaterEqual(opcodes["test"], 2)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        count = 0
        for m in range(len(messages)-1):
            if messages[m]["opcode"] == "sample":
                count += len(messages[m]["data"])
            if messages[m]["opcode"] == "test":
                # Check an exact block length
                self.assertEqual(count % self.test_generator._block_length, 0)
                # Check consecutive
                self.assertEqual(messages[m]["opcode"],
                                 messages[m+1]["opcode"])
                break

        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "intra_consecutive", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Minimum number of messages.
        self.assertGreaterEqual(len(messages), 3)
        # Only sample and at least 2 test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        self.assertGreaterEqual(opcodes["test"], 2)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        count = 0
        for m in range(len(messages)-1):
            if messages[m]["opcode"] == "sample":
                count += len(messages[m]["data"])
            if messages[m]["opcode"] == "test":
                # Check not an exact block length
                self.assertNotEqual(
                    count % self.test_generator._block_length, 0)
                # Check consecutive
                self.assertEqual(messages[m]["opcode"],
                                 messages[m+1]["opcode"])
                break

        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "intra_multiple", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample and at least 4 test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        self.assertGreaterEqual(opcodes["test"], 4)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        count = 0
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])

        messages = self.test_generator._inter_intra_tests(
            self.seed, "pytest", "eof", lambda: {"opcode": "test", "data": None})
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample and at least 4 test messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["test"])
        # Start with sample
        self.assertEqual(messages[0]["opcode"], "sample")
        # End with test
        self.assertEqual(messages[-1]["opcode"], "test")
        # Not an exact block length
        self.assertNotEqual(count % self.test_generator._block_length, 0)

    def test_time(self):
        messages = self.test_generator.time(self.seed, "zero")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one time message
        self.assertGreaterEqual(opcodes["time"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "time":
                self.assertEqual(messages[m]["data"], 0)

        messages = self.test_generator.time(self.seed, "positive")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one time message
        self.assertGreaterEqual(opcodes["time"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "time":
                self.assertGreater(messages[m]["data"], 0)

        messages = self.test_generator.time(self.seed, "maximum")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one time message
        self.assertGreaterEqual(opcodes["time"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "time":
                self.assertEqual(messages[m]["data"],
                                 self.test_generator.TIME_MAX)

        messages = self.test_generator.time(self.seed, "intra_multiple")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample and at least 4 time messages
        self.assertEqual(len(messages), opcodes["sample"]+opcodes["time"])
        self.assertGreaterEqual(opcodes["time"], 4)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        # End with time
        self.assertEqual(messages[-1]["opcode"], "time")
        # Not an exact block length
        self.assertNotEqual(count % self.test_generator._block_length, 0)

        # Check time supports _intra_inter tests (detail already tested).
        messages = self.test_generator.time(self.seed, "single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.time(self.seed, "intra_single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.time(self.seed, "consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.time(self.seed, "intra_consecutive")
        self.assertGreater(len(messages), 0)

    def test_time_calculation(self):
        for subcase in ["interval_change", "time_change", "minimum_increment"]:
            messages = self.test_generator.time_calculation(self.seed, subcase)
            count, minimum, maximum, opcodes = self._analyse_messages(messages)
            # At least one of each of:
            self.assertGreaterEqual(opcodes["time"], 1)
            self.assertGreaterEqual(opcodes["sample_interval"], 1)
            self.assertGreaterEqual(opcodes["flush"], 1)
            # At least one block
            self.assertGreaterEqual(count, self.test_generator._block_length)

    def test_opcode_interaction(self):
        messages = self.test_generator.opcode_interaction(
            self.seed, "time_flush")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least 2 of each of:
        self.assertGreaterEqual(opcodes["time"], 2)
        self.assertGreaterEqual(opcodes["flush"], 2)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)

        messages = self.test_generator.opcode_interaction(
            self.seed, "time_discontinuity")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least 2 of each of:
        self.assertGreaterEqual(opcodes["time"], 2)
        self.assertGreaterEqual(opcodes["discontinuity"], 2)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)

        messages = self.test_generator.opcode_interaction(
            self.seed, "time_metadata")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least 2 of each of:
        self.assertGreaterEqual(opcodes["time"], 2)
        self.assertGreaterEqual(opcodes["metadata"], 2)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)

        messages = self.test_generator.opcode_interaction(
            self.seed, "all_opcodes")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least 6 of each of:
        self.assertGreaterEqual(opcodes["sample"], 6)
        self.assertGreaterEqual(opcodes["time"], 6)
        self.assertGreaterEqual(opcodes["sample_interval"], 6)
        self.assertGreaterEqual(opcodes["flush"], 6)
        self.assertGreaterEqual(opcodes["discontinuity"], 6)
        self.assertGreaterEqual(opcodes["metadata"], 6)
        # At least 6 blocks
        self.assertGreaterEqual(count, 6*self.test_generator._block_length)

    def test_sample_interval(self):
        messages = self.test_generator.sample_interval(self.seed, "zero")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one sample_interval message
        self.assertGreaterEqual(opcodes["sample_interval"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "sample_interval":
                self.assertEqual(messages[m]["data"], 0)

        messages = self.test_generator.sample_interval(self.seed, "positive")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one sample_interval message
        self.assertGreaterEqual(opcodes["sample_interval"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "sample_interval":
                self.assertGreater(messages[m]["data"], 0)

        messages = self.test_generator.sample_interval(self.seed, "maximum")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one sample_interval message
        self.assertGreaterEqual(opcodes["sample_interval"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "sample_interval":
                self.assertEqual(messages[m]["data"],
                                 self.test_generator.TIME_MAX)

        # Check sample_interval supports _intra_inter tests (detail already tested).
        messages = self.test_generator.sample_interval(self.seed, "single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.sample_interval(
            self.seed, "intra_single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.sample_interval(
            self.seed, "consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.sample_interval(
            self.seed, "intra_consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.sample_interval(
            self.seed, "intra_multiple")
        self.assertGreater(len(messages), 0)

    def test_flush(self):
        # Check flush supports _intra_inter tests (detail already tested).
        messages = self.test_generator.flush(self.seed, "single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.flush(self.seed, "intra_single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.flush(self.seed, "consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.flush(self.seed, "intra_consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.flush(self.seed, "intra_multiple")
        self.assertGreater(len(messages), 0)

    def test_discontinuity(self):
        # Check discontinuity supports _intra_inter tests (detail already tested).
        messages = self.test_generator.discontinuity(self.seed, "single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.discontinuity(self.seed, "intra_single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.discontinuity(self.seed, "consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.discontinuity(
            self.seed, "intra_consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.discontinuity(
            self.seed, "intra_multiple")
        self.assertGreater(len(messages), 0)

    def test_metadata(self):
        messages = self.test_generator.metadata(self.seed, "none")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # No metadata message
        self.assertGreaterEqual(opcodes["metadata"], 0)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)

        messages = self.test_generator.metadata(self.seed, "zero")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one metadata message
        self.assertGreaterEqual(opcodes["metadata"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "metadata":
                self.assertEqual(messages[m]["data"]["id"], 0)
                self.assertEqual(messages[m]["data"]["value"], 0)

        messages = self.test_generator.metadata(self.seed, "positive")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one metadata message
        self.assertGreaterEqual(opcodes["metadata"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "metadata":
                self.assertGreater(messages[m]["data"]["id"], 0)
                self.assertGreater(messages[m]["data"]["value"], 0)

        messages = self.test_generator.metadata(self.seed, "maximum")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # At least one metadata message
        self.assertGreaterEqual(opcodes["metadata"], 1)
        # At least one block
        self.assertGreaterEqual(count, self.test_generator._block_length)
        for m in range(len(messages)-1):
            self.assertNotEqual(messages[m]["opcode"], messages[m+1]["opcode"])
            if messages[m]["opcode"] == "metadata":
                self.assertEqual(messages[m]["data"]["id"],
                                 self.test_generator.METADATA_ID_MAX)
                self.assertEqual(messages[m]["data"]["value"],
                                 self.test_generator.METADATA_VALUE_MAX)

        # Check metadata supports _intra_inter tests (detail already tested).
        messages = self.test_generator.metadata(self.seed, "single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.metadata(self.seed, "intra_single")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.metadata(self.seed, "consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.metadata(self.seed, "intra_consecutive")
        self.assertGreater(len(messages), 0)
        messages = self.test_generator.metadata(self.seed, "intra_multiple")
        self.assertGreater(len(messages), 0)

    def test_soak(self):
        messages = self.test_generator.soak(self.seed, "sample_only")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample messages
        self.assertEqual(len(messages), opcodes["sample"])
        # Check minimum number of samples output
        self.assertGreaterEqual(
            count, self.test_generator._block_length*self.test_generator._soak_blocks)
        for m in messages:
            # Minimum size 1
            self.assertGreaterEqual(len(m["data"]), 1)
            # Maximum size
            self.assertLessEqual(
                len(m["data"]), self.test_generator.MESSAGE_SIZE_LONGEST)

        messages = self.test_generator.soak(self.seed, "all_opcodes")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Check minimum number of samples output
        self.assertGreaterEqual(
            count, self.test_generator._block_length*self.test_generator._soak_blocks)
        # At least one of each opcode
        for opcode, count in opcodes.items():
            if opcode != "test":
                self.assertGreaterEqual(count, 1)

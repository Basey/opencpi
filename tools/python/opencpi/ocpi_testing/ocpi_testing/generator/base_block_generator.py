#!/usr/bin/env python3

# Base class for generating block based sample input data.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Base class for all block based generators."""

import abc
import math
import random
import decimal
from itertools import permutations

import opencpi.ocpi_protocols as ocpi_protocols
from .base_generator import BaseGenerator


class BaseBlockGenerator(BaseGenerator):
    """Block Based Generator Base Class.

    A block processing generator for a unit-under-test which processes
    after a "block" length of input samples, so that the number of samples
    in a sample opcode is significant, and the total number of samples is
    ideally a multiple of the block size (with consideration for flush
    and discontinuity opcodes which reset the internal buffer).

    This is intended to be used:
    * For block or frame based components where samples are processed in
      chunks in an internal buffer and only produce output when the buffer
      is complete, the output will be a block of samples (usually the
      same size as the input block).
    * For decimating components where the "decimation factor" number of
      samples has to be received to produce one output sample.
      An "output block" is just one sample.
    * Any other component with an internal buffer and run_length processing.
    """

    def __init__(self, block_length=0, number_of_soak_blocks=0):
        """Initialisation parameters.

        Args:
            block_length (int): The processing block length of the component.
            number_of_soak_blocks (int): The number of block_length
                samples to inject during the soak test.

            Dummy defaults are required so that the class can be default
            initialised by get_generate_arguments class (instantiates
            solely to get the list of valid cases (self.CASES)).
        """
        super().__init__()
        self._block_length = int(block_length)
        self._soak_blocks = number_of_soak_blocks

        # Block based additions to test case list
        self.CASES["time_calculation"] = self.time_calculation
        self.CASES["opcode_interaction"] = self.opcode_interaction

        self.reset()

    def reset(self):
        """Reset generate parameters."""
        # Initialise the samples sent count
        self._number_samples_sent = 0

        # Initialise wave parameters.
        self._frequencies = None
        self._amplitudes = None
        self._phases = None

    def _get_wave_parameters(self, subcase=None):
        """Get parameters for the sinusoidal wave generator.

        Generate a number of sinusoidal waves that can be used so that the
        output is a superposition of several sinusoidal waves.

        This is the same logic as the non block based generators but split
        from the "typical" function, so other functions can use it too.

        Args:
            subcase (str): Subcase for which to generate waveforms.
                           (Not used in base class. Allows child classes to
                           specialise on subcase).
        """
        if self._frequencies is None:
            # Use log normal distribution to get a bias towards more waves than the
            # modal
            number_of_waves = int(random.lognormvariate(
                math.log(self.TYPICAL_NUMBER_OF_WAVES_MODAL),
                math.log(self.TYPICAL_NUMBER_OF_WAVES_DISTRIBUTION_WIDTH)))
            if number_of_waves < self.TYPICAL_NUMBER_OF_WAVES_MIN:
                number_of_waves = self.TYPICAL_NUMBER_OF_WAVES_MIN
            if number_of_waves > self.TYPICAL_NUMBER_OF_WAVES_MAX:
                number_of_waves = self.TYPICAL_NUMBER_OF_WAVES_MAX

            self._frequencies = [0] * number_of_waves
            self._phases = [0] * number_of_waves
            self._amplitudes = [0] * number_of_waves

            for wave in range(number_of_waves):
                self._frequencies[wave] = random.gauss(
                    self.TYPICAL_FREQUENCY_MEAN,
                    self.TYPICAL_FREQUENCY_DISTRIBUTION_WIDTH)
                if self._frequencies[wave] < self.TYPICAL_FREQUENCY_MIN:
                    self._frequencies[wave] = self.TYPICAL_FREQUENCY_MIN
                if self._frequencies[wave] > self.TYPICAL_FREQUENCY_MAX:
                    self._frequencies[wave] = self.TYPICAL_FREQUENCY_MAX

                self._phases[wave] = random.uniform(0, 2 * math.pi)

                self._amplitudes[wave] = abs(random.gauss(
                    self.TYPICAL_AMPLITUDE_MEAN / number_of_waves,
                    self.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH))

            # Scale amplitude if maximum is exceeded
            amplitude_sum = sum(self._amplitudes)
            if amplitude_sum > self.TYPICAL_MAXIMUM_AMPLITUDE:
                scale_factor = self.TYPICAL_MAXIMUM_AMPLITUDE / amplitude_sum
                self._amplitudes = [amplitude *
                                    scale_factor for amplitude in self._amplitudes]

        return (self._frequencies, self._phases, self._amplitudes)

    def _generate_sine_wave_samples(self, number_of_samples, subcase=None):
        """Generate samples with sinusoidal values.

        Combine a number of sinusoidal waves together, so that the output is
        the superposition of several sinusoidal waves.
        This should be the same as the non block based generator, except that
        the parameters are not regenerated on each generate call.

        This base implementation generates a list of float/double. A child
        class for other types (int,complex) should override this.

        Args:
            number_of_samples (int): The number of random values to
                be generated.
            subcase (str): Subcase for which to generate waveforms. (Not used
                in base class - allows child classes to specialise on subcase).

        Returns:
            List of samples
        """
        (frequencies, phases, amplitudes) = self._get_wave_parameters(subcase)
        data = [0.0] * number_of_samples
        for index in range(len(data)):
            sample = self._number_samples_sent + index

            for frequency, phase, amplitude in zip(frequencies, phases, amplitudes):
                data[index] += amplitude * \
                    math.sin(2 * math.pi * frequency * sample + phase)

        # Increment sample number for next call
        self._number_samples_sent += len(data)

        return data

    @abc.abstractmethod
    def _generate_random_samples(self, number_of_samples):
        """Generate samples with (limited range) random values.

        This must be implemented by the protocol specific child class.

        Args:
            number_of_samples (int): The number of random values to
                be generated.

        Returns:
            List of the generated random values.
        """
        raise NotImplementedError(
            "_generate_random_samples() must be implemented by child class")

    def _get_sample_values(self, number_of_samples=None):
        """Generate samples using "default" generator.

        Subclassing of base_generator._get_sample_values()
        Virtual function wrapper to the block based generators.

        Args:
            number_of_samples (int, optional): Number of samples to generate.

        Returns:
            List of samples.
        """
        if number_of_samples is None:
            number_of_samples = self.SAMPLE_DATA_LENGTH
        return self._generate_random_samples(number_of_samples)

    def _full_scale_random_sample_values(self, number_of_samples=None):
        """Generate samples using "default" generator.

        Subclassing of base_generator._full_scale_random_sample_values()
        Virtual function wrapper to the block based generators.

        Args:
            number_of_samples (int, optional): Number of samples to generate.

        Returns:
            List of samples.
        """
        raise NotImplementedError(
            "_full_scale_random_sample_values() must be implemented by child class")

    def _generate_sample_opcodes(self, samples_to_add):
        """Generates sample opcodes using the default sample generator.

        Args:
            samples_to_add (int): Number of samples to generate.

        Returns:
            sequence of sample opcode messages
        """
        messages = []
        if samples_to_add < 1:
            samples_to_add = 1
        while samples_to_add > 0:
            message_length = min(samples_to_add, self.MESSAGE_SIZE_LONGEST)
            messages.append(
                {"opcode": "sample", "data": self._get_sample_values(message_length)})
            samples_to_add -= message_length
        return messages

    def _split_samples(self, samples):
        """Split samples into max message size chunks.

        Args:
            samples (list): Samples to split

        Returns:
            List of samples of message size maximum.
        """
        for i in range(0, len(samples), self.MESSAGE_SIZE_LONGEST):
            yield samples[i:i + self.MESSAGE_SIZE_LONGEST]

    def _split_samples_into_opcodes(self, samples):
        """Split samples into maximum buffer size sample opcode messages.

        Args:
            samples (list of numbers): Samples to use.

        Returns:
            sequence of sample opcode messages
        """
        return [{"opcode": "sample", "data": s} for s in self._split_samples(samples)]

    def typical(self, seed, subcase):
        """Generate a sample message with typical data inputs.

        Combine a number of sinusoidal waves together, so that the output is
        the superposition of several sinusoidal waves.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the typical case or the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self._block_length, self.SAMPLE_DATA_LENGTH)
        data = self._generate_sine_wave_samples(number_of_samples, subcase)
        return self._split_samples_into_opcodes(data)

    def property(self, seed, subcase):
        """Generate sample message to allow property value testing.

        Generates random data for the input port.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the property testing.
        """
        random.seed(seed)
        return self._generate_sample_opcodes(self._block_length)

    def input_stressing(self, seed, subcase):
        """Generate sample message to allow property value testing.

        Generates random data for the input port.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the property testing.
        """
        random.seed(seed)
        number_of_samples = 2*max(self._block_length, self.SAMPLE_DATA_LENGTH)
        return self._generate_sample_opcodes(number_of_samples)

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self._block_length, self.SAMPLE_DATA_LENGTH)

        if subcase == "random":
            data = self._generate_random_samples(number_of_samples)
            return self._split_samples_into_opcodes(data)

        elif subcase == "sinusoidal":
            data = self._generate_sine_wave_samples(number_of_samples, subcase)
            return self._split_samples_into_opcodes(data)

        else:
            raise ValueError(f"Unexpected subcase of {subcase} for sample()")

    def message_size(self, seed, subcase):
        """Messages when testing a port's handling of different message sizes.

        These subcases ensure enough data is sent to complete a block.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the message size testing.
        """
        random.seed(seed)
        if subcase == "shortest":
            number_of_messages = (self._block_length //
                                  self.MESSAGE_SIZE_SHORTEST) + 1
            return [{"opcode": "sample",
                     "data": self._get_sample_values(
                         self.MESSAGE_SIZE_SHORTEST)}
                    for _ in range(number_of_messages)]

        elif subcase == "longest":
            number_of_messages = self.MESSAGE_SIZE_NUMBER_OF_MESSAGES
            if self._block_length > (self.MESSAGE_SIZE_LONGEST * number_of_messages):
                number_of_messages = (self._block_length //
                                      self.MESSAGE_SIZE_LONGEST) + 1
            return [{"opcode": "sample",
                     "data": self._get_sample_values(
                         self.MESSAGE_SIZE_LONGEST)}
                    for _ in range(number_of_messages)]

        elif subcase == "different_sizes":
            # Base class will output self.SAMPLE_DATA_LENGTH samples, repeat
            # enough times to ensure at least one block length is generated.
            number_of_repeats = math.ceil(
                self._block_length / self.SAMPLE_DATA_LENGTH)
            messages = []
            for _ in range(number_of_repeats):
                messages.extend(super().message_size(seed, subcase))
            return messages

        else:
            raise ValueError(
                f"Unexpected subcase of {subcase} for message_size()")

    def _inter_intra_tests(self, seed, case, subcase, opcode_function):
        """Opcode/sample size testing.

        Messages when testing a port's handling opcodes between and within
            buffers.

        Call from opcode specific test case (else clause)

        subcase single: Inject opcode, at the start and
            after processing a buffer.

        subcase intra_single: Partially fill buffer, insert test_opcode,
            then finish with a full buffer.

        subcase consecutive: Send a full buffer, 2 test_opcodes consecutively,
            then finish with a full buffer.

        subcase intra_consecutive: Partially fill buffer, 2 consecutive opcodes
            then finish with a full buffer.

        subcase intra_multiple: Test test_opcode with various sizes of partially filled buffers.
            (Assumes test_opcode will reset buffer - this should be implemented appropriately
             for opcodes that do not reset so that the correct behaviour of a partly filled
             buffer can be tested - for example, see the 'time' implementation).

        subcase eof: Test test_opcode received at EOF after only a single sample
            has been received.

        Args:
            seed (int): The seed value to use for random number generation.
            case (str): Name of the case (for diagnostics)
            subcase (str): Name of the subcase messages are to be generated for.
            opcode_function (lambda): Function to generate opcode tuple. Note: This is
                passed as a lambda rather than a fixed opcode tuple. This lambda can
                be call multiple times to insert opcodes.  A fixed tuple would insert
                the same data multiple times. Using a lambda allows generate different
                random data for each opcode.

        Returns:
            Messages for the requested test subcase.
        """
        random.seed(seed)
        if subcase == "single":
            messages = [opcode_function()]
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "intra_single":
            messages = self._generate_sample_opcodes(self._block_length//2)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "consecutive":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append(opcode_function())
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "intra_consecutive":
            messages = self._generate_sample_opcodes(self._block_length//2)
            messages.append(opcode_function())
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "intra_multiple":
            messages = self._generate_sample_opcodes(self._block_length-1)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length+1)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length*3//2)
            messages.append(opcode_function())
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "eof":
            messages = self._generate_sample_opcodes(1)
            messages.append(opcode_function())
            return messages

        else:
            raise ValueError(
                f"Unexpected subcase of {subcase} for {case}")

    def time(self, seed, subcase):
        """Messages when testing a port's handling of time messages.

        Usually called via ``self.generate()``.

        Intended to be called with block_length < input buffer to misalign
        input buffers and output buffers (i.e. block_length < 16k/sample size),
        and with block_length a multiple of 4 (to allow quarter buffers).

        subcase zero: Test zero time (block boundary).

        subcase positive: Test positive time (block boundary).

        subcase maximum: Test maximum time value on block boundary.

        subcase intra_multiple: Test time injected after each quarter
        of a buffer length.

        plus all _inter_intra_tests() subcases not already defined above

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested time test subcase.
        """
        random.seed(seed)
        if subcase == "zero":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "positive":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "time", "data": random.uniform(
                0.0, float(self.TIME_MAX))})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "maximum":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "time", "data": self.TIME_MAX})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "intra_multiple":
            messages = self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages += self._generate_sample_opcodes(1)
            # Test held timestamp at EOF
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
            return messages

        else:
            return self._inter_intra_tests(seed, "time", subcase, lambda:
                                           {"opcode": "time", "data": random.uniform(
                                               0.0, float(self.TIME_MAX))})

    def time_calculation(self, seed, subcase):
        """Test time calculations using sample_interval and time.

        Usually called via ``self.generate()``.

        subcase interval_change: sample_interval change mid-buffer

        subcase time_change: time change mid-buffer

        subcase minimum_increment: Test smallest valid increment
            (40 bit fractional part)

        plus all time() tests but with sample_increment set first
            so that non-zero increment time calculations take place

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested time_calculation test subcase.
        """
        random.seed(seed)
        if subcase == "interval_change":
            messages = [{"opcode": "sample_interval", "data": 1.0}]
            if self._block_length == 2:
                messages += self._generate_sample_opcodes(1)
                messages.append({"opcode": "time", "data": 2.0})
                messages.append({"opcode": "sample_interval", "data": 2.5})
            else:
                messages += self._generate_sample_opcodes(
                    self._block_length//4)
                messages.append({"opcode": "time", "data": 2.0})
                messages += self._generate_sample_opcodes(
                    self._block_length//4)
                messages.append({"opcode": "sample_interval", "data": 2.5})
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "time", "data": 2.0})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "sample_interval", "data": 0.25})
            messages.append({"opcode": "flush", "data": None})
            return messages

        elif subcase == "time_change":
            messages = [{"opcode": "sample_interval", "data": 1.0}]
            messages.append({"opcode": "time", "data": 2.0})
            messages += self._generate_sample_opcodes(self._block_length//2)
            messages.append({"opcode": "time",
                             "data": random.uniform(0.0, float(self.TIME_MAX))})
            messages += self._generate_sample_opcodes(self._block_length*2)
            messages.append({"opcode": "flush", "data": None})
            return messages

        elif subcase == "minimum_increment":
            messages = [{"opcode": "sample_interval",
                         "data": decimal.Decimal(2**-40)}]
            messages += self._generate_sample_opcodes(self._block_length//2)
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length//2)
            messages += self._generate_sample_opcodes(self._block_length*2)
            messages.append({"opcode": "flush", "data": None})
            return messages

        else:
            messages = [{"opcode": "sample_interval", "data": 1.0}]
            messages += self._inter_intra_tests(seed, "time_calculation", subcase, lambda:
                                                {"opcode": "time", "data": random.uniform(
                                                    0.0, float(self.TIME_MAX))})
            messages.append({"opcode": "flush", "data": None})
            return messages

    def opcode_interaction(self, seed, subcase):
        """Test opcode ordering/re-ordering.

        Usually called via ``self.generate()``.

        subcase time_flush: Test time processing when a flush is received.

        subcase time_discontinuity: Test time processing when a discontinuity
            is received.

        subcase time_metadata: Test time processing when metadata are received.

        subcase all_opcodes: Tests all permutations of the non-sample opcodes
            sent when a buffer is partially filled.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested time test subcase.
        """
        random.seed(seed)
        messages = [{"opcode": "sample_interval", "data": 1.0}]

        if subcase == "time_flush":
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "flush", "data": None})
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length)
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time", "data": 0.0})
            messages.append({"opcode": "flush", "data": None})
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length//4)
            # Sample after time completed by flush (edge case).
            messages.append({"opcode": "flush", "data": None})
            messages += self._generate_sample_opcodes(self._block_length*2)
            return messages

        elif subcase == "time_discontinuity":
            messages.append({"opcode": "time", "data": 0.0})
            messages.append({"opcode": "discontinuity", "data": None})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time", "data": 0.0})
            messages.append({"opcode": "discontinuity", "data": None})
            messages += self._generate_sample_opcodes(self._block_length)
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "discontinuity", "data": None})
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length*2)
            return messages

        elif subcase == "time_metadata":
            messages.append({"opcode": "time", "data": 0.0})
            messages.append({"opcode": "metadata", "data": {
                "id": random.randint(1, self.METADATA_ID_MAX),
                "value": random.randint(1, self.METADATA_VALUE_MAX)}})
            messages += self._generate_sample_opcodes(self._block_length//4)
            messages.append({"opcode": "time", "data": 0.0})
            for _ in range(4):
                messages.append({"opcode": "metadata", "data": {
                    "id": random.randint(1, self.METADATA_ID_MAX),
                    "value": random.randint(1, self.METADATA_VALUE_MAX)}})
            messages += self._generate_sample_opcodes(self._block_length)
            messages += self._generate_sample_opcodes(self._block_length//4)
            for _ in range(4):
                messages.append({"opcode": "metadata", "data": {
                    "id": random.randint(1, self.METADATA_ID_MAX),
                    "value": random.randint(1, self.METADATA_VALUE_MAX)}})
            messages.append({"opcode": "time", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length//2)
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "all_opcodes":
            opcode_functions = [
                lambda: {"opcode": "flush", "data": None},
                lambda: {"opcode": "discontinuity", "data": None},
                lambda: {"opcode": "time", "data": random.uniform(
                    0.0, float(self.TIME_MAX))},
                lambda: {"opcode": "sample_interval", "data": random.uniform(
                    0.0, float(self.SAMPLE_INTERVAL_MAX))},
                lambda: {"opcode": "metadata", "data": {"id": 0, "value": 0}},
                lambda: {"opcode": "metadata", "data": {
                    "id": self.METADATA_ID_MAX,
                    "value": self.METADATA_VALUE_MAX}}]
            for functions in list(permutations(opcode_functions)):
                messages += self._generate_sample_opcodes(
                    self._block_length//4)
                for opcode in list(functions):
                    messages.append(opcode())
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "flush", "data": None})
            return messages

        else:
            messages += self._inter_intra_tests(seed, "opcode_interaction", subcase, lambda:
                                                {"opcode": "time", "data": random.uniform(
                                                    0.0, float(self.TIME_MAX))})
            return messages

    def sample_interval(self, seed, subcase):
        """Messages when testing a port's handling of sample_interval.

        Usually called via ``self.generate()``.

        subcase zero: Zero value sample_interval (for compatibility with
        non-block based generators).

        subcase positive: Positive value sample_interval (for compatibility with
        non-block based generators).

        subcase maximum: Maximum value sample_interval (for compatibility with
        non-block based generators).

        plus all _inter_intra_tests() subcases

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested sample_interval test subcase.
        """
        random.seed(seed)
        messages = []
        if subcase == "zero":
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "sample_interval", "data": 0.0})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "positive":
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "sample_interval", "data": random.uniform(
                0.0, float(self.SAMPLE_INTERVAL_MAX))})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "maximum":
            messages += self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "sample_interval",
                             "data": self.SAMPLE_INTERVAL_MAX})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        else:
            return self._inter_intra_tests(seed, "sample_interval", subcase, lambda:
                                           {"opcode": "sample_interval", "data": random.uniform(
                                               0.0, float(self.SAMPLE_INTERVAL_MAX))})

    def flush(self, seed, subcase):
        """Messages when testing a port's handling of flush messages.

        Usually called via ``self.generate()``.

        Delegates all to _inter_intra_tests()

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested flush test subcase.
        """
        return self._inter_intra_tests(seed, "flush", subcase, lambda:
                                       {"opcode": "flush", "data": None})

    def discontinuity(self, seed, subcase):
        """Messages when testing a port's handling of discontinuity messages.

        Usually called via ``self.generate()``.

        Delegates all to _inter_intra_tests()

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested discontinuity test subcase.
        """
        return self._inter_intra_tests(seed, "discontinuity", subcase, lambda:
                                       {"opcode": "discontinuity", "data": None})

    def metadata(self, seed, subcase):
        """Messages when testing a port's handling of metadata opcodes.

        Usually called via ``self.generate()``.

        subcase none: Input 2 buffers of samples with no metadata. This is
                      for components that generate metadata opcodes.

        subcase zero: Test metadata with zero values.

        subcase positive: Test metadata with positive values.

        subcase maximum: Test metadata with maximum values.

        plus all _inter_intra_tests() subcases

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested sample metadata test subcase.
        """
        random.seed(seed)
        if subcase == "none":
            messages = self._generate_sample_opcodes(self._block_length*2)
            return messages

        elif subcase == "zero":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append(
                {"opcode": "metadata", "data": {"id": 0, "value": 0}})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "positive":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "metadata", "data": {
                "id": random.randint(1, self.METADATA_ID_MAX),
                "value": random.randint(1, self.METADATA_VALUE_MAX)}})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        elif subcase == "maximum":
            messages = self._generate_sample_opcodes(self._block_length)
            messages.append({"opcode": "metadata", "data": {
                "id": self.METADATA_ID_MAX,
                "value": self.METADATA_VALUE_MAX}})
            messages += self._generate_sample_opcodes(self._block_length)
            return messages

        else:
            return self._inter_intra_tests(seed, "metadata", subcase, lambda:
                                           {"opcode": "metadata", "data": {
                                               "id": random.randint(1, self.METADATA_ID_MAX),
                                               "value": random.randint(1, self.METADATA_VALUE_MAX)}})

    def soak(self, seed, subcase):
        """Soak test by generating random sized sample messages.

        Usually called via ``self.generate()``.

        This is based on the non-block base class version, but ensures
        that the number and size of messages are such that sufficient
        frames worth of samples are generated.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the requested subcase.
        """
        random.seed(seed)
        if self._soak_blocks < 1:
            print("Warning: number_of_soak_blocks is not set")

        number_of_samples = self._block_length * self._soak_blocks
        # Average one message per frame but to avoid exceeding the
        # maximum message size limit the calculation to half this.
        # This will generate more than one message per frame for
        # larger block sizes.
        number_of_messages = round(random.gauss(
            number_of_samples /
            min(self._block_length, self.MESSAGE_SIZE_LONGEST/2),
            self.SOAK_ALL_OPCODE_STANDARD_DEVIATION_NUMBER_OF_MESSAGES))
        if number_of_messages < 1:
            number_of_messages = 1

        messages = []

        if subcase == "sample_only":
            message_lengths = [0] * (number_of_messages - 1)

            # The total of all messages should be number_of_samples
            for index in range(number_of_messages - 1):
                remaining_messages = number_of_messages - index - 1
                # Set a maximum which ensures that all messages have at least
                # one data sample.
                maximum_message_length = min(number_of_samples
                                             - sum(message_lengths)
                                             - remaining_messages,
                                             self.MESSAGE_SIZE_LONGEST/2)
                message_lengths[index] = random.randint(
                    1, maximum_message_length)

            # The final message will have a length to ensure that all sample
            # messages have a length which totals number_of_samples.
            # If this exceeds the maximum, split over multiple messages.

            while sum(message_lengths) < number_of_samples:
                remaining_samples = number_of_samples - sum(message_lengths)
                message_lengths.append(
                    min(remaining_samples, self.MESSAGE_SIZE_LONGEST))

            for length in message_lengths:
                messages.append({
                    "opcode": "sample",
                    "data": self._full_scale_random_sample_values(length)})

        elif subcase == "all_opcodes":
            sample_data_included = 0
            # Calculate a message size to generate approximately number_of_messages.
            # This assumes chances of sample opcode is boosted.
            # Number of messages will be larger when max message size < block length.
            sample_message_size = min(
                number_of_samples // number_of_messages,
                self.MESSAGE_SIZE_LONGEST)

            # List of opcodes to randomly include, bias towards sample opcode as
            # this triggers most of the functionality.
            opcodes = [*ocpi_protocols.OPCODES.values()] + ["sample"]*4
            while sample_data_included < number_of_samples:
                opcode = random.choice(opcodes)
                # As opcode keys are opcode names and values, if an int this
                # will be the value. In which case get the name.
                if isinstance(opcode, int):
                    opcode = ocpi_protocols.OPCODES[opcode]

                if opcode == "sample":
                    messages.append({
                        "opcode": "sample",
                        "data": self._full_scale_random_sample_values(
                            sample_message_size)})
                    sample_data_included = (sample_data_included +
                                            sample_message_size)
                elif opcode == "time":
                    messages.append({
                        "opcode": "time",
                        "data": random.uniform(self.TIME_MIN, float(self.TIME_MAX))})
                elif opcode == "sample_interval":
                    messages.append({
                        "opcode": "sample_interval",
                        "data": random.uniform(self.SAMPLE_INTERVAL_MIN, 1.0)
                    })
                elif opcode == "flush":
                    messages.append({"opcode": "flush", "data": None})
                elif opcode == "discontinuity":
                    messages.append({"opcode": "discontinuity", "data": None})
                elif opcode == "metadata":
                    messages.append({
                        "opcode": "metadata",
                        "data": {
                            "id": random.randint(0, self.METADATA_ID_MAX),
                            "value": random.randint(0, self.METADATA_VALUE_MAX)
                        }})
                else:
                    raise ValueError(
                        f"Unexpected opcode of {opcode} for soak()")

            # Ensure any partial frame is processed before finishing.
            messages.append({"opcode": "flush", "data": None})

        else:
            raise ValueError(f"Unexpected subcase of {subcase} for soak()")

        return messages
